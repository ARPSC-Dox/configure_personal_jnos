#!/bin/sh
ZONE="MIZ047"
COUNTY="MIC111"
CRONJ="/home/jnos_system_account/cronjobs"
WWORK="/home/jnos_system_account/workareas/weather"

cd ${WORK}

rm -f tmp.txt

wget -q -O tmp.txt "http://www.crh.noaa.gov/showsigwx.php?warnzone=${ZONE}&warncounty=${COUNTY}&product1=Hazardous+Weather+Outlook"

${CRONJ}/striphwo <tmp.txt >/jnos/public/nws/hwo.txt

rm tmp.txt


