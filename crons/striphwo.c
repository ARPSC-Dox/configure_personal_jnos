/*! \file striphwo.c

 \brief Strip HTML from Hazardous Weather Outlook

*/
#include <stdio.h>
#include <string.h>

//! Buffer for file reads
char szBuffer[1024];

//! main - Strip HTML from Hazardous Weather Outlook

/*! striphwo reads stdin looking for an HTML level three
header tag (&lt;h3&gt;).  If found, it passes the input to 
stdout until the end of preformatting tag is found (&lt;/pre&gt;).
If the header tag was never found it writes a note to
stdout that no HWO was active.

Pseudocode:
\code
  nDone=0
  while not nDone
    get a line of input
    if EOF
      Write not HWO found to stdout
      return
    if header tag
      nDone = 1
  nDone = 0
  while not nDone
    Read a line of input
    if EOF
      nDone = 1
    else
      if /pre tag
        nDone = 1
      else if $$
        nDone = 1
      else
        If '<' not in buffer
          write line to stdout
\endcode
*/

int main()
{
  int nDone;
  int nIsWarning;
  int nLines;
  char *p;

  nDone = 0;
  nIsWarning = 0;
  nLines = 0;
  // Find <h3> signalling the start of the warning
  while ( !nDone )
    {
      gets(szBuffer);
      if ( feof(stdin) )
	{
	  printf("\nNo hazardous weather outlook currently active\n");
	  return 0;
	}
      if ( !strncmp("<h3>",szBuffer,4) )
	nDone = 1;
    }

  // Now skip through the rest of the buffer
  nDone = 0;
  while( !nDone )
    {
      gets(szBuffer);
      if ( feof(stdin) )
	nDone = 1;
      else
	{
	  // If we found the /pre, we are at the end of the warning
	  if ( !strncmp("</pre>",szBuffer,4) )
	    {
	      nIsWarning++;
	      nDone = 1;
	    }
	  // Normally, a $$ marks the end of the warning
	  else if ( !strncmp("$$",szBuffer,2) )
	    {
	      nIsWarning++;
	      nDone = 1;
	    }
	  // Otherwise, we may have something to shoe
	  else
	    {
	      // Check for HTML tags
	      p = strchr(szBuffer,'<');
	      // If no tag, it's a plain text line to show
	      if ( p == NULL )
		{
		  puts(szBuffer);
		  nLines++;
		}
	    }
	}
    }
  if ( (!nIsWarning || nLines<4) )
    printf("\nNo hazardous weather outlook currently active\n\n");
  //printf("\bnLines=%d.\n\n",nLines);
  return 0;
}

