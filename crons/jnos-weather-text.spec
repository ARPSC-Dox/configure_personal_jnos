Summary:        Text weather reports for JNOS
Name:           jnos-weather-text
Version:        0.2
Release:        1%{?dist}
License:        GPLv2
Group:          System Environment/Base
URL:            http://ares-mi.org/downloads/
Source0:        %{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root
BuildArch:      armv6hl


%description
Provide test weather reports for a JNOS PBBS

%prep
%setup -q

%build
make -k striphwo
make -k striptpt

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/home/jnos_system_account/cronjobs
mkdir -p $RPM_BUILD_ROOT/home/jnos_system_account/workareas/weather
install -m 755 striphwo $RPM_BUILD_ROOT/home/jnos_system_account/cronjobs
install -m 755 striptpt $RPM_BUILD_ROOT/home/jnos_system_account/cronjobs
install -m 755 NWSforecast.sh $RPM_BUILD_ROOT/home/jnos_system_account/cronjobs
install -m 755 hwo.sh $RPM_BUILD_ROOT/home/jnos_system_account/cronjobs
chgrp -R 1001 $RPM_BUILD_ROOT/home/jnos_system_account/*
chown -R 1001 $RPM_BUILD_ROOT/home/jnos_system_account/*
mkdir -p $RPM_BUILD_ROOT/jnos/public/nws
mkdir -p $RPM_BUILD_ROOT/jnos/www
chgrp 1001 $RPM_BUILD_ROOT/jnos/www
chgrp 1001 $RPM_BUILD_ROOT/jnos/public
chgrp 1001 $RPM_BUILD_ROOT/jnos/public/nws
chmod 755 $RPM_BUILD_ROOT/jnos/public
chmod 755 $RPM_BUILD_ROOT/jnos/public/nws
systemctl enable crond.service
systemctl start crond.service
./makecron

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/home/jnos_system_account/cronjobs/hwo.sh
/home/jnos_system_account/cronjobs/NWSforecast.sh
/home/jnos_system_account/cronjobs/striphwo
/home/jnos_system_account/cronjobs/striptpt
/home/jnos_system_account/workareas/
/jnos/www
/jnos/public/nws

%changelog
* Sun Jul 28 2013 John J. McDonough <jjmcd@fedoraproject.org> - 0.2-1
- Initial
