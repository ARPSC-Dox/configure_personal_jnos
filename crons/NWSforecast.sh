#!/bin/sh
#
#  Script gets text forecast from NWS
#
#  wb8rcr - 2006-05-07
#  wb8fyr - 2013-03-14 did some mods to accommodate changes at NWS web page formats
#  wb8fyr - 2013-03-14 note changes in script show before (as comments) in comparison to new
#  wb8rcr - 2013-07-28 Change account from jnos to jnos_system_account
#

LAT="43.6001243591309"
LON="-84.2027969360352"
NWS="dtx"
CRONJ="/home/jnos_system_account/cronjobs"
WORK="/home/jnos_system_account/workareas/weather"

cd ${WORK}

# Grab the file from NWS
wget -O tmp.txt "http://www.crh.noaa.gov/ifps/MapClick.php?FcstType=text&textField1=${LAT}&textField2=${LON}&site=${NWS}&Radius=0&CiTemplate=0&TextType=1" >/dev/null

# Do some of the editing
# updated 2013-03-14 to do one additional tweak to accomodate no hazardous outlook and new html at noaa.gov.
# Compare to following that was before on 2012-07-18 (was corrected at wb8fyr at this 2012 date)

sed -e"s|<br></div><br>|\n<br>Under Hazardous Weather Outlook\n|ig" tmp.txt|sed -e"s|><b>Ove|>\n<b>Ove|ig"|sed -e"s|><b>Thi|>\n<b>Thi|ig"|sed -e"s|><b>To|>\n<b>To|ig"|sed -e"s|<td width=\"80%|\n<td width=\"80%|ig"|sed -e"s|Issued by|\nIssued by|ig"|sed -e"s|\<br\>||ig"|sed -e"s|\<b\>||ig"|sed -e"s|\<\/b\>||ig"|sed -e's"<>""ig'|sed -e's"</>""ig' >Report.txt

# updated 2012-07-18 from the following for previous html code at noaa.gov
# sed -e"s|\<br\>||ig" tmp.txt|sed -e"s|\<b\>||ig"|sed -e"s|\<\/b\>||ig"|sed -e's"<>""ig'|sed -e's"</>""ig' >Report.txt
# Do more of the editing
${CRONJ}/striptpt >fcast.txt
# Send it off to JNOS
cp fcast.txt /jnos/public/nws
rm -f Report.txt fcast.txt tmp.txt


