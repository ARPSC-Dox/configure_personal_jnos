/* $Id: striptpt.c,v 1.4 2008-03-24 09:53:28-04 jjmcd Exp jjmcd $ */
/*! \file striptpt.c

 \brief Strip HTML from NWS text forecast

    This program takes an html version of an NWS text report and
    strips out the HTML that was left behind by a pass from sed.

    The result is intended to be a text forecast
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//! Name of output file
#define FILENAME "Report.txt"

//! Buffer for file read
char szBuffer[2048];
//! Buffer for resulting output stream
char szOutBuf[2048];

int nRecursionDepth;

//! moveOut - word wrap the output

/*! moveOut first examines the output buffer and removes any trailing
whitespace.  If the buffer is less than 61 characters long, the 
buffer is written to stdout.

If the buffer is longer, then moveOut begins looking for a space
after position 60.  When a space is found it is replaced with a
line terminator and the output written to stdout.  The remainder
of the buffer is then moved to the front, and moveOut then calls
itself to process the remainder.
 */
void moveOut(void)
{
  char *p;
  int n;

  // Limit recursion in case of error
  nRecursionDepth++;
  if ( nRecursionDepth > 10 )
      return;

  n = strlen(szOutBuf);
  if ( szOutBuf[n-1] == '\n' )
    {
      szOutBuf[n-1] = '\0';
      n--;
    }

  if ( n < 61 )
    puts(szOutBuf);
  else
    {
      p = &szOutBuf[60];
      while ( *p>' ' && p>szOutBuf )
	{
	  //	  printf("*p='%c' (%d)\n",*p,*p);
	  p++;
	}
      *p = '\0';
      puts(szOutBuf);
      p++;
      // This only works because we know that
      // p-szOutBuf > 2
      // Be sure we haven't moved past null
      if ( p-szOutBuf < n )
	{
	  strcpy(szOutBuf,"\t");
	  if ( *p == ' ' ) p++;
	  strcat(szOutBuf,p);
	  
	  moveOut();
	}
    }
}
//! main - Remove HTML formatting from the NWS forecast

/*! striptpt reads a file from which some of the formatting has already
been removed with \c sed.  \s striptpt reads the entire file passing
the input to the output, and processing according to a simple
state machine described below:
\code
  nS = 1
  while not EOF
    get a line from the file

    state 1:
        if the line begins with "<td "
          Remove the two HTML tags beginning the line
          Write the line to output
          nS = 2
        else
          do nothing
    state 2:
        remove "<TR>" from the line
        Write the line to output
        nS = 3
    state 3:
        if the line DOES NOT begin with a '<' character
          Write the line to output
          nS = 4
        else
          do nothing
    state 4:
        if the line begins with '<'
          nS = 5
        else
          Write the line to output
    state 5:
        do nothing
  end while
\endcode
 */
int main()
{
  FILE *f;
  int nS;
  char *p,*q;

  f = fopen(FILENAME,"r");

  nS = 1;    // Set initial state
  // Loop through the file
  while ( !feof(f) )
    {
      memset(szBuffer,0,sizeof(szBuffer));
      fgets(szBuffer,sizeof(szBuffer),f);
      if ( !feof(f) )
	{
	  // Clear recursion counter
	  nRecursionDepth=0;
	  // 4: Pass the actual report.  Keep passing lines to output
	  //    until a line show up starting with a '<'.  Once that
	  //    happens, move to state 5 which will simply run out the
	  //    rest of the input adding nothing to the output
	  if ( nS==4 )
	    {
	      if ( szBuffer[0]=='<' )
		{
		  nS = 5;
		}
	      else
		{
		  sprintf(szOutBuf,"%s",szBuffer);
		  moveOut();
		}
	    }
	  // 3: Remove the eye candy between the heading and the
	  //    actual body of the report. The first line that
	  //    DOES NOT begin with a '<' marks the real report
	  if ( nS==3 )
	    {
	      if ( szBuffer[0]!='<' )
		{
		  sprintf(szOutBuf,"%s",szBuffer);
		  moveOut();
		  nS = 4;
		}
	    }
	  // 2: Pass the single line "Issued by" but remove <TR>
	  if ( nS==2 )
	    {
	      p = strchr(szBuffer,'<');
	      *p = '\0';
	      sprintf(szOutBuf,"%s\n\n",szBuffer);
	      moveOut();
	      nS = 3;
	    }
	  // 1: Don't pass anything until the <td that marks the start
	  //    of the actual forecast.  Everything before is eye candy.
	  if ( nS==1 )
	    {
	      if (!strncmp(szBuffer,"<td ",4))
		{
		  // Remove the 2 tags that precede the text
		  q = strchr(szBuffer,'>');
		  q++;
		  p = strchr(q,'>');
		  p++;
		  strcpy(szBuffer,p);
		  // And remove the trailing </font>
		  q = strchr(szBuffer,'<');
		  *q = '\0';
		  sprintf(szOutBuf,"%s\n",szBuffer);
		  moveOut();
		  nS = 2;
		}
	    }
	}
    }

}
