#!/usr/bin/python -tt
#
# writesedscript.py
#
# Writes out a script for SED that edits the configuration files.
# Pretty dull stuff.

def write_sed( quad, city, zip, lccall, amprip, call, mynode, hgnode, hgip,
                freq, county, grid, linip, tunip, mac, device, ether,
                sysop, sysoppw, cid, ccode ) :

    f = open( './templates/sedscript','w')

    astr = "s/%QUAD%/" + quad + "/\n";
    sastr = str(astr)
    f.write(sastr)

    astr = "s/%CITY%/" + city + "/\n";
    sastr = str(astr)
    f.write(sastr)

    astr = "s/%LCCALL%/" + lccall + "/\n";
    sastr = str(astr)
    f.write(sastr)

    astr = "s/%AMPRIP%/" + amprip + "/\n";
    sastr = str(astr)
    f.write(sastr)

    astr = "s/%ZIP%/" + zip + "/\n";
    sastr = str(astr)
    f.write(sastr)

    astr = "s/%CALL%/" + call + "/\n";
    sastr = str(astr)
    f.write(sastr)
    f.write(sastr)
    f.write(sastr)
    f.write(sastr)

    astr = "s/%MYNODE%/" + mynode + "/\n";
    sastr = str(astr)
    f.write(sastr)

    astr = "s/%HG%/" + hgnode + "/\n";
    sastr = str(astr)
    f.write(sastr)

    astr = "s/%HGIP%/" + hgip + "/\n";
    sastr = str(astr)
    f.write(sastr)

    astr = "s/%FREQ%/" + freq + "/\n";
    sastr = str(astr)
    f.write(sastr)

    astr = "s/%COUNTY%/" + county + "/\n";
    sastr = str(astr)
    f.write(sastr)

    astr = "s/%CCODE%/" + ccode + "/\n";
    sastr = str(astr)
    f.write(sastr)

    astr = "s/%CNUM%/" + str(cid) + "/\n";
    sastr = str(astr)
    f.write(sastr)

    astr = "s/%GRID%/" + grid + "/\n";
    sastr = str(astr)
    f.write(sastr)

    astr = "s/%LINIP%/" + linip + "/\n";
    sastr = str(astr)
    f.write(sastr)

    astr = "s/%TUNIP%/" + tunip + "/\n";
    sastr = str(astr)
    f.write(sastr)

    astr = "s/%MAC%/" + mac + "/\n";
    sastr = str(astr)
    f.write(sastr)

    astr = "s/%DEVICE%/" + device + "/\n";
    sastr = str(astr)
    f.write(sastr)

    astr = "s/%ETHER%/" + ether + "/\n";
    sastr = str(astr)
    f.write(sastr)

    astr = "s/%SYSOP%/" + sysop + "/\n";
    sastr = str(astr)
    f.write(sastr)

    astr = "s/%SYSOPPW%/" + sysoppw + "/\n";
    sastr = str(astr)
    f.write(sastr)

    f.closed

    print "sed script written"
