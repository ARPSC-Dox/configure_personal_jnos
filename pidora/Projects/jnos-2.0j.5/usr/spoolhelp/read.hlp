     R[ead] <msg_number_or_range> [<msg_number_or_range> . . .]
     RH
     RM
     <msg_number>
     <ENTER>

     Each of these  commands allows you to read a message (or messages) from
     the current mail area.  To read a specific message, you may either type
     "read #" or just  the number by itself.  If there is a specific list of
     messages  you are interested  in  (determined  by the use of the L[ist]
     command, for  instance),  you can  enter  the list  of  message numbers
     (separated by spaces) on the  "read" command-line.  You may specify a
     range of message numbers by placing a "-" between the first and last
     message numbers of the range.  No intervening spaces are allowed.
     You can also simply advance sequentially through the messages by just
     pressing the <ENTER> key.  This will display the next message in order.
     The "read" command displays only an abbreviated portion of the mail
     headers.  If you want all the header lines displayed, use the V[erbose]
     command instead.

     RH is for sysop use, and reads each held message in the current area.
     After reading a held message, the sysop is asked if the hold can be
     released, making the message readable by other users.  The msg hold
     feature is optional.  

     RM will display, without interruption, all unread messages.


EXAMPLES
     read 3 5        (Display only messages 3 and 5)
     4               (Display message 4)
     <CR>            (Display next message)
     read 4-7        (Display only messages 4, 5, 6 and 7)

