     S[end] <user>[ @ <host>] [< <from_addr>] [$<bulletin_id>]
     SR [msg_number]
     SF <user>[ @ <host>] [< <from_addr>] [$<bulletin_id>]
     SC

     The send command allows you to enter a message  and send it to a user at
     either this system, or some other system on the network. The "from_addr"
     and "bulletin_id" fields are  for special use and won't be covered here.
     The "S" command  may also be followed by "P", "B", or any  other message
     type  you use  (e.g. SP wb7xxx @ n7xxx for personal messages, SB for
     bulletins, ST for traffic messages, etc.).

     "SR" allows you to "reply" to either the current message or the message
     number specified.  The subject will be copied and the reply will be sent
     to the address it was sent from, as well as to any Cc'd users other than
     yourself.

     "SF" will forward a copy of the current message to the user specified.
     You will be asked for a subject, and the incorporated message will be
     preceded and followed by a line indicating the message was forwarded.

     "SC" allows you to send a personal message to more than one user. The
     system will prompt with "Cc: ", which allows you to add more users to be
     sent Carbon-copies of the message.  Separate users on the Cc line with
     commas, spaces or tabs.

EXAMPLES
     send kf7xx            (Send a message to the local user, kf7xx)
     s kf7xx @ wb7xxx      (Send a message to kf7xx at the wb7xxx host)
     sr 3                  (Reply to message number 3)
     sf n7aaa%n7bbb@w7ccc  (Forward current msg to n7aaa at n7bbb via w7ccc)
     sc wg7j               (Send with Carbon-copy to others)
     Cc: ka7ehk, n7dva@n7dva


