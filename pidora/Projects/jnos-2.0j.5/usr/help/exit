
exit [return_code]                                      Default: 0

     Causes the JNOS program to terminate when at the NET> prompt.
When shelled to DOS, causes a return to the NET> prompt. When terminating
the program, an "Are you sure?" query is given. Enter "y(es) <cr>" to end
the program.  Any other response returns to JNOS.

     If a <return_code> numeric value is provided, this value is returned
to the caller of Jnos.  This is typically a batch file, which may then
take action depending on this code.  For example:
	...
:rerun
	jnos110i  -u1 -g2
	if errorlevel 100	goto remote_exit
	if errorlevel 99	reboot
	if errorlevel 1		goto rerun
	goto exit
:remote_exit
	...
:exit


In this example, issuing "exit 99" would run the reboot command, "exit 1"
would restart Jnos, and "exit" or "exit 0" would exit the batch file that
invoked Jnos.  The remote command (c.v.) when given the exit parameter,
will use a return code of 100.
