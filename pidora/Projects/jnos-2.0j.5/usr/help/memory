
memory <subcommands>

     These commands are used for memory allocation.

     
    memory freelist

     Display the storage allocator free list.  Each entry consists of
     a starting segment, in hex, and a size, in decimal bytes.
     
     
    memory ibufsize [<size>]

     Display or set the size of the buffers in the  interrupt
     buffer  pool.   The size  should  be  set to the largest type of
     buffer plus a header size of 8.  For example: If your ax.25 is
     the only interface and a packet length of 512 is defined, the
     ibufsize  should  be 512 + 72 + 8 = 592 .  The 72 is the ax.25
     header (source , destination, 8 digipeaters, 1 control byte and 1
     pid byte).  Default is obtained from the value of IBUFSIZE in config.h.
     See also the section on INTERFACE BUFFERS (in the JNOS40 CONFIGURATION
     MANUAL, the relevant section is called 'Interface Buffers').
     
    memory minalloc [<bytes>]

     Set the minimum number of bytes to allocate per malloc() call.
     Setting a small value (32 or 64) might allow the realloc scheme
     to work more effectively by preventing excessive memory
     fragmentation.  Default = 0, no minimum allocation size.
     
     
    memory nibufs [<number>]

     Display or set the number of interrupt buffer pool buffers.
     If the number of buffers is set, the statistics in the 'memory
     status' display are reset for number of interrupt buffer fails.
     The minimum available value is set to the requested  number  of
     buffers.  A rule of thumb for the number of buffers is to watch
     the statistics and keep a minimum of 2  free  buffers.  Increase
     or decrease as required. Default is 10.  See also the section on
     INTERFACE BUFFERS (in the JNOS40 CONFIGURATION MANUAL, the relevant
     section is called 'Interface Buffers').
     
     
    memory sizes

     Display a histogram of storage allocator requested sizes.  Each
     histogram bin is a binary order of magnitude (i.e., a factor of
     2).
     
     
    memory status

     Display a summary of storage allocator statistics.
     
     heap size 52560, avail 12880 (24%), morecores 150, coreleft 5872
     

     The first line shows the total size of the internal heap, the
     amount of memory available on the internal heap with the
     percentage of the total heap size, next the number of times
     memory has been requested from the Operating System, and the
     amount of memory the OS has left over.
     
     allocs 16706, frees 16389 (diff 317), alloc fails 0, invalid
     frees 0
     
     Next, the number of times memory has been allocated, and has been
     freed is shown.  The difference is the number of buffers
     currently allocated.  Alloc fails show up when the system is
     running out of memory resources.
     Invalid frees mean that memory was overwritten, and indicates the
     system is about to lose sanity...

          garbage collections yellow 0, red 0
     
     Garbage collections free memory from network control structures
     that could not have otherwise been freed.  Yellow garbage
     collections are started when the total available memory, i.e.
     avail+coreleft, becomes smaller then memthresh.  Red garbage
     collections indicate that available memory got below memthresh/2
     
     interrupts-off calls to malloc 0, free 0
     
     These indicate calls to the memory allocator with interrupts off.
     These requests are handled using the interrupt buffer pool, which
     is configured by setting memory ibufsize and nibufs.
     
     Intqlen 9 Ibufsize 600 Iminfree 9 Ibuffail 0 Imaxrq 580
     
     This line shows the current number of interrupts buffers in the
     interrupts buffer pool, the size of each buffer, the minimum
     number of free buffers, and a count of times no interrupt buffer of
     sufficient size was available when one was needed.  If Iminfree gets
     close to, or becomes zero, you should increase the buffer pool size
     with the 'memory nibuf' command.  The statistics are reset when this
     command is executed.  Also shown is the size of the largest request
     for an interrupt buffer, to help establish an optimum Ibufsize value.
     If Ibuffail is non-zero, it indicates that packet(s) were dropped; to
     prevent this, increase ibufsize (if Imaxrq > Ibufsize), and/or increase
     nibufs (if Iminfree = 0).

     
     
    memory thresh [<size>]

     Displays or sets the memory threshold size in bytes.  If
     free memory gets below this value, no more new connections can be
     started and no new connections will be accepted.  This is an
     attempt to preserve the system's sanity.
     

