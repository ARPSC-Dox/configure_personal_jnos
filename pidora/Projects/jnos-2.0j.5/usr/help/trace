
trace [<iface> [off | <btio> [outfile]]

     Controls packet tracing by the interface drivers. Specific bits
enable  tracing  of the various interfaces and the amount of
information produced.  Tracing is controlled on a per-interface basis;
without arguments, 'trace' gives a list of all defined interfaces and
their tracing status.  Output can be limited to a single interface by
specifying it, and the control flags can be changed by specifying them
as well.  Trace control flags may be followed by <outfile>, a path to
a disk file to contain the tracing output.

     The flags are given as a hexadecimal number which is interpreted
as follows:

B    - Broadcast & RawDump selector
  1  - Broadcast filter flag. If set, only packets specifically
       addressed to this node will be traced; broadcast packets will
       not be displayed.
  2  - If this bit is set, a "raw" dump style is selected, for those
       interfaces which support it (e.g., ppp).
  4  - If this bit is set, include polls in the output trace of polled-kiss
       interfaces.
T    - Controls type of tracing:
  0  - Protocol headers are decoded, but data is not displayed
  1  - Protocol headers are decoded, and data (but not the headers
       themselves) are displayed  as ASCII characters, 64 characters
       per line.  Unprintable characters are displayed as periods.
  2  - Protocol headers are decoded, and the entire packet (headers
       AND data) is also displayed  in hexadecimal and ASCII, 16 chars
       per line.
  3  - A minimal display of headers and data is produced, provided Jnos
       was compiled with MONITOR #define'd.
I    - Enable tracing of input packets if 1, disable if 0
O    - Enable tracing of output packets if 1, disable if 0


          Example:
          # Trace all packets on port1 and display with headers:
          trace port1 0111
          # Trace all lan non-broadcast received packets to a file:
          trace lan 1210 d:\lan_ipt.trc


