Summary:        Packet BBS
Name:           jnos
Version:        2.0j.6
Release:        1%{?dist}
License:        GPLv2
Group:          System Environment/Base
URL:            http://www.langelaar.net/projects/jnos2/
Source0:        %{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root
BuildArch:      armv6hl

BuildRequires:  ncurses-devel

%description
Raspberry Pi specific install of Maiko Langelaar's jnos 2.0.
Specific to Michigan, presumes TNC-Pi

%prep
%setup -q

%build
make

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_bindir}
install -m 755 jnos $RPM_BUILD_ROOT%{_bindir}
mkdir -p $RPM_BUILD_ROOT%{_datarootdir}/jnos
mkdir -p $RPM_BUILD_ROOT%{_datarootdir}/jnos/help
mkdir -p $RPM_BUILD_ROOT%{_datarootdir}/jnos/spoolhelp
mkdir -p $RPM_BUILD_ROOT%{_datarootdir}/jnos/templates
mkdir -p $RPM_BUILD_ROOT%{_var}/log/jnos/wwwlogs
mkdir -p $RPM_BUILD_ROOT%{_var}/spool/jnos/mail
mkdir -p $RPM_BUILD_ROOT%{_var}/spool/jnos/mqueue
install -m 644 usr/help/* $RPM_BUILD_ROOT%{_datarootdir}/jnos/help/
install -m 644 usr/spoolhelp/* \
 	$RPM_BUILD_ROOT%{_datarootdir}/jnos/spoolhelp/
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/jnos
install -m 644 etc/access.www $RPM_BUILD_ROOT%{_sysconfdir}/jnos/
install -m 644 etc/areas $RPM_BUILD_ROOT%{_sysconfdir}/jnos/
install -m 644 etc/forward.bbs $RPM_BUILD_ROOT%{_sysconfdir}/jnos/
install -m 644 etc/mail $RPM_BUILD_ROOT%{_sysconfdir}/jnos/
install -m 644 etc/names.dat $RPM_BUILD_ROOT%{_sysconfdir}/jnos/
install -m 644 etc/nos.cfg $RPM_BUILD_ROOT%{_sysconfdir}/jnos/
install -m 644 etc/rewrite $RPM_BUILD_ROOT%{_sysconfdir}/jnos/
install -m 644 etc/users.dat $RPM_BUILD_ROOT%{_sysconfdir}/jnos/
install -m 644 templates/autoexec.nos \
	$RPM_BUILD_ROOT%{_datarootdir}/jnos/templates/
install -m 644 templates/ax25 \
	$RPM_BUILD_ROOT%{_datarootdir}/jnos/templates/
install -m 644 templates/channel.dat \
	$RPM_BUILD_ROOT%{_datarootdir}/jnos/templates/
install -m 644 templates/converse \
	$RPM_BUILD_ROOT%{_datarootdir}/jnos/templates/
install -m 644 templates/convmotd.txt \
	$RPM_BUILD_ROOT%{_datarootdir}/jnos/templates/
install -m 644 templates/ftpmotd.txt \
	$RPM_BUILD_ROOT%{_datarootdir}/jnos/templates/
install -m 644 templates/ftpusers \
	$RPM_BUILD_ROOT%{_datarootdir}/jnos/templates/
install -m 644 templates/mbox \
	$RPM_BUILD_ROOT%{_datarootdir}/jnos/templates/
install -m 644 templates/motd.txt \
	$RPM_BUILD_ROOT%{_datarootdir}/jnos/templates/
install -m 644 templates/popusers \
	$RPM_BUILD_ROOT%{_datarootdir}/jnos/templates/
install -m 644 templates/tcpip \
	$RPM_BUILD_ROOT%{_datarootdir}/jnos/templates/
install -m 644 templates/tun \
	$RPM_BUILD_ROOT%{_datarootdir}/jnos/templates/
install -m 644 templates/encap \
	$RPM_BUILD_ROOT%{_datarootdir}/jnos/templates/
install -m 644 templates/resolv \
	$RPM_BUILD_ROOT%{_datarootdir}/jnos/templates/
install -m 644 share/Configure_Personal_JNOS.glade \
	$RPM_BUILD_ROOT%{_datarootdir}/jnos/
install -m 755 share/Configure_Personal_JNOS.py \
	$RPM_BUILD_ROOT%{_datarootdir}/jnos/
install -m 644 share/fillstores* \
	$RPM_BUILD_ROOT%{_datarootdir}/jnos/
install -m 644 share/writesedscript* \
	$RPM_BUILD_ROOT%{_datarootdir}/jnos/
install -m 755 share/writescript \
	$RPM_BUILD_ROOT%{_datarootdir}/jnos/

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_usr}/bin
%{_datarootdir}/jnos
%{_sysconfdir}/jnos
%{_var}/log/jnos/
%{_var}/spool/jnos/

%changelog
* Thu Jun 13 2013 John J. McDonough <jjmcd@fedoraproject.org> - 2.0j.6-1
- Add encap, resolv, update nos.cfg
* Thu Jun 13 2013 John J. McDonough <jjmcd@fedoraproject.org> - 2.0j.5-1
- Correct converse, writescript, add getEthers back into .py
* Thu Jun 13 2013 John J. McDonough <jjmcd@fedoraproject.org> - 2.0j.4-2
- Create some additional needed directories
* Thu Jun 13 2013 John J. McDonough <jjmcd@fedoraproject.org> - 2.0j.4-1
- Template files
* Tue Jun 11 2013 John J. McDonough <jjmcd@fedoraproject.org> - 2.0j.3-1
- Reorganize tar
* Tue Jun 11 2013 John J. McDonough <jjmcd@fedoraproject.org> - 2.0j.2-1
- Include templates in tar
* Tue Jun 11 2013 John J. McDonough <jjmcd@fedoraproject.org> - 2.0j.1-3
- Better use of macros
* Mon Jun 10 2013 John J. McDonough <jjmcd@fedoraproject.org> - 2.0j.1-2
- Correct protections on executable
* Mon Jun 10 2013 John J. McDonough <jjmcd@fedoraproject.org> - 2.0j.1-1
- yum localinstall --nogpgcheck 
-   http://ares-mi.org/repo/jnos-2.0j.1-2.rpfr.armv6hl.rpm
- Package Maikos work
