/*
 * JNOS 2.0
 *
 * $Id: snmpd.c,v 1.2 2012/03/20 16:36:26 ve4klm Exp $
 *
 * 17Dec2010, Maiko Langelaar, VE4KLM - took very little time to write this
 * original code, which is what makes JNOS so cool as a development platform,
 * it's so easy to add new TCP or UDP services to a JNOS system - very nice.
 *
 * Basic SNMP server so I can get MRTG to poll JNOS itself for iface data,
 * as of 20Dec2010, number of packets is all that JNOS collects in the form
 * of iface statistics - I expect to add RX and TX Bytes to 'struct iface'
 * at some point in the near future - so for now, packets/sec is all we
 * can do ...
 *
 * 31Jan2011, Maiko, information I found that might be useful - To know the
 * interface speed, MRTG poll the mib ifSpeed. The default interface speed
 * is equal to ifSpeed, but if you've modified the speed with the bandwidth
 * command, the ifSpeed is also modified.
 *
 * 01Feb2011, Maiko, reorganizing the code, make it more logical, at some
 * point I should use a MIB lookup table approach, but with the number of
 * MIBs that I am dealing with right now it's not worth the effort (yet).
 *
 * Make the code so that it *survives* an snmpwalk - service was exiting,
 * would help to use 'continue' instead of 'break', also warn if not v1,
 * or if it's anything other than a GetRequest PDU - make more robust.
 *
 * snmpwalk -v1 -c jnos 192.168.1.201
 *
 * snmpget -v1 -c jnos 192.168.1.201 1.3.6.1.2.1.2.2.1.16.9
 *
 * Lastly, a NEW nos_log_peerless() function for UDP based clients ...
 * 
 */

#include "global.h"

#include <ctype.h>

#ifdef	SNMPD

#include "usock.h"

/*
 * IF-MIB::ifInOctets.N  -> 6.1.2.1.2.2.1.10 .N
 * IF-MIB::ifOutOctets.N -> 6.1.2.1.2.2.1.16 .N
 *
 * SNMPv2-MIB::sysUpTime.0 -> 6.1.2.1.1.3.0
 * SNMPv2-MIB::sysName.0 -> 6.1.2.1.1.5.0
 *
 */

static char ifInOctets[9] = { 43, 6, 1, 2, 1, 2, 2, 1, 10 };
static char ifOutOctets[9] = { 43, 6, 1, 2, 1, 2, 2, 1, 16 };

static char sysUpTime[8] = { 43, 6, 1, 2, 1, 1, 3, 0 };
static char sysName[8] = { 43, 6, 1, 2, 1, 1, 5, 0 };

static int Snmpd = -1;

static int snmpd_debug = 0;	/* 04Jan2011, Maiko, debug flag for logging */

enum mibtype { Nothing, InOctets, OutOctets, UpTime, Name };	/* 01Feb2011, Maiko, Haven't used enums for ages */

#define	J2_SNMPD_DEBUG

#ifdef	J2_SNMPD_DEBUG

/* 01Feb2011, Maiko (VE4KLM), Something different, new LOG function */
extern void nos_log_peerless (struct sockaddr_in *fsock, char *fmt, ...);

static void snmpd_dump (struct sockaddr_in *fsock, int len, unsigned char *ptr)
{
	static char dumpbuffer[500];

	char *sptr = dumpbuffer;

	int cnt = 0;

	if (!len) return;

	while (cnt < len)
	{
		if (!isprint (*ptr))
			sptr += sprintf (sptr, "[%02x]", *ptr);
		else
			*sptr++ = *ptr;

		cnt++;
		ptr++;
	}

	*sptr = 0;

	nos_log_peerless (fsock, "DUMP (%d) >>>%s<<<", len, dumpbuffer);
}

#endif

/* 01Feb2011, Maiko, Help me better identify what's coming in */
static void log_ber_type6 (struct sockaddr_in *fsock, int blen, char *bptr, char *desc)
{
	char btmp[80], *ptr = btmp, *bptr2 = bptr;
	int bcnt;

	btmp[0] = 0;

	for (bcnt = 0; bcnt < blen; bcnt++, bptr2++)
		ptr += sprintf (ptr, " %d", (int)(*bptr2));

	nos_log_peerless (fsock, "MIB [%s ] %s", btmp, desc);
}

int snmpd1 (int argc, char **argv, void *p)
{
    struct sockaddr_in lsocket,fsock;
    int cnt, len, ber_type, ber_len, vblen, pdlen, olen;
    unsigned char data[200], *ber_ptr, *vblenptr, *pdulenptr, *olenptr;
	unsigned char idata[256], *idptr;
	unsigned char odata[256], *odptr;
    enum mibtype mtype;
    struct mbuf *bp;

	extern char hextochar (char*);

    j2psignal (Curproc, 0);
    chname (Curproc,"SNMP listener");
    lsocket.sin_family = AF_INET;
    lsocket.sin_addr.s_addr = INADDR_ANY;
    lsocket.sin_port = 161;

    if ((Snmpd = j2socket(AF_INET,SOCK_DGRAM,0)) == -1)
    {
        j2tputs(Nosock);
        return -1;
    }

    j2bind(Snmpd,(char *)&lsocket,sizeof(lsocket));

	while (1)
	{
        len = sizeof(fsock);

        if (recv_mbuf(Snmpd,&bp,0,(char *)&fsock,&len) == -1)
            break;

		/* make darn sure these pointers are reset here !!! */
		idptr = idata; odptr = odata;

		if ((*odptr++ = PULLCHAR (&bp)) != 0x30) break;

		olenptr = odptr;	/* update this after response is finished */
		odptr++;

 		len = PULLCHAR (&bp);

		pullup (&bp, idata, len);

#ifdef	J2_SNMPD_DEBUG
		if (snmpd_debug)
			snmpd_dump (&fsock, len, idata);
#endif
		/* SNMP Version */

		if ((*odptr++ = *idptr++) != 0x02)
			continue;
		if ((*odptr++ = *idptr++) != 0x01)
			continue;
		if ((*odptr++ = *idptr++) != 0x00)
		{
			nos_log_peerless (&fsock, "only SNMPv1 is supported");
			continue;
		}

		/* SNMP Community String */

		if ((*odptr++ = *idptr++) != 0x04)
			continue;

		*odptr++ = len = *idptr++;

		if (snmpd_debug)
			nos_log_peerless (&fsock, "community [%.*s]", len, idptr);

		memcpy (odptr, idptr, len);
		odptr += len;
		idptr += len;

		olen = 5 + len;

		/* SNMP PDU - Type and Length */

		if (*idptr++ != 0xa0)
		{
			nos_log_peerless (&fsock, "only GetRequest PDU is supported");
			continue;
		}

		*odptr++ = 0xa2;

		pdulenptr = odptr;	/* update this after pdu is finished */
		odptr++;
 		idptr++;

		/* Request ID, Error, and Error Index */

		for (pdlen = 0, cnt = 0; cnt < 3; cnt++)
		{
			if ((*odptr++ = *idptr++) != 0x02) break;

			*odptr++ = len = *idptr++;

			memcpy (odptr, idptr, len);
			odptr += len;
			idptr += len;

			pdlen += len;
			pdlen += 2;
		}

		/* Varbind List - Type and Length */

		if ((*odptr++ = *idptr++) != 0x30)
			continue;

		vblenptr = odptr;	/* update this after all varbind's are added */
		odptr++;
 		len = *idptr++;

		memcpy (data, idptr, len);
		idptr += len;

		/* Parse the varbind list */

		ber_ptr = data;

		vblen = 0;

		while (len > 0)
		{
			ber_type = *ber_ptr++;
			len--;

			ber_len = *ber_ptr++;
			len--;

			if (snmpd_debug)
				nos_log_peerless (&fsock, "type %d len %d", ber_type, ber_len);

			if (ber_type == 0x30)
				continue;

			/* 01Feb2011, Maiko, Start to better organize this - should actually have a lookup table */
			if (ber_type == 6)
			{
				/* 01Feb2011, Maiko, Help me better identify what's coming in */
				if (snmpd_debug)
					log_ber_type6 (&fsock, ber_len, ber_ptr, "");

				if (ber_len == 10)
				{
					if (!memcmp (ifInOctets, ber_ptr, 9))
						mtype = InOctets;
					else if (!memcmp (ifOutOctets, ber_ptr, 9))
						mtype = OutOctets;
					else
					{
						/* should log any MIB for which we don't have support for */
						log_ber_type6 (&fsock, ber_len, ber_ptr, "needs a support function");
						mtype = Nothing;
					}

					if (mtype != Nothing)
					{
						int iface_cnt = 0;
						int iface_number;
    					struct iface *ifp;
						char htmp[9];

						/* last byte specifies the interface number */
						iface_number = *(ber_ptr + 9);
 
						/* 19Dec2010, now add actual iface data */
        				for (ifp = Ifaces; ifp != NULLIF; iface_cnt++, ifp = ifp->next)
							if (iface_cnt == iface_number)
								break;

						/* Counter32 minus the interface number */
						if (mtype == InOctets)
						{
							/* Varbind type and len */
							*odptr++ = 0x30;
							*odptr++ = 0x12;

							/* OID */
							*odptr++ = 0x06;
							*odptr++ = 0x0a;
							memcpy (odptr, ber_ptr, 0x0a);
							odptr += 0x0a;

							/* Value */
							*odptr++ = 0x41;
							*odptr++ = 0x04;

							if (ifp)
								sprintf (htmp, "%08x", ifp->rawrecbytes);
							else
								sprintf (htmp, "%08x", 0);

							if (snmpd_debug)
								nos_log_peerless (&fsock, "rawreccnt [%s]", htmp);

							*odptr++ = hextochar (&htmp[0]);
							*odptr++ = hextochar (&htmp[2]);
							*odptr++ = hextochar (&htmp[4]);
							*odptr++ = hextochar (&htmp[6]);

							vblen += 0x14;
						}

						/* Counter32 minus the interface number */
						else if (mtype == OutOctets)
						{
							/* Varbind type and len */
							*odptr++ = 0x30;
							*odptr++ = 0x12;

							/* OID */
							*odptr++ = 0x06;
							*odptr++ = 0x0a;
							memcpy (odptr, ber_ptr, 0x0a);
							odptr += 0x0a;

							/* Value */
							*odptr++ = 0x41;
							*odptr++ = 0x04;

							if (ifp)
								sprintf (htmp, "%08x", ifp->rawsndbytes);
							else
								sprintf (htmp, "%08x", 0);

							if (snmpd_debug)
								nos_log_peerless (&fsock, "rawsndcnt [%s]", htmp);

							*odptr++ = hextochar (&htmp[0]);
							*odptr++ = hextochar (&htmp[2]);
							*odptr++ = hextochar (&htmp[4]);
							*odptr++ = hextochar (&htmp[6]);

							vblen += 0x14;
						}
					}
				}			/* end of if ber_len == 10 */

				else if (ber_len == 8)
				{
					/* TimeTicks */
					if (!memcmp (sysUpTime, ber_ptr, 8))
					{
						if (snmpd_debug)
							nos_log_peerless (&fsock, "sysUpTime");

						/* Varbind type and len */
						*odptr++ = 0x30;
						*odptr++ = 0x10;

						/* OID */
						*odptr++ = 0x06;
						*odptr++ = 0x08;
						memcpy (odptr, ber_ptr, 0x08);
						odptr += 0x08;

						/* Value */
						*odptr++ = 0x43;
						*odptr++ = 0x04;

					/* 20Dec2010, Maiko (VE4KLM), Uptime is zero for now */
						*odptr++ = 0x00;
						*odptr++ = 0x00;
						*odptr++ = 0x00;
						*odptr++ = 0x00;

						vblen += 18;
					}
					/* Octet String */
					else if (!memcmp (sysName, ber_ptr, 8))
					{
						extern char *Hostname;

						int hlen = strlen (Hostname);

						if (snmpd_debug)
							nos_log_peerless (&fsock, "sysName [%s]", Hostname);

						/* Varbind type and len */
						*odptr++ = 0x30;
						*odptr++ = (unsigned char)(hlen + 12);

						/* OID */
						*odptr++ = 0x06;
						*odptr++ = 0x08;
						memcpy (odptr, ber_ptr, 0x08);
						odptr += 0x08;

						/* Value */
						*odptr++ = 0x04;
						*odptr++ = (unsigned char)strlen (Hostname);
						odptr += sprintf (odptr, "%s", Hostname);

						vblen += hlen;

						vblen += 14;
					}
				}
			}

			ber_ptr += ber_len;

			len -= ber_len;
		}

		*vblenptr = vblen;	/* make sure to update varbind list length */

		*pdulenptr = vblen + 2 + pdlen;	/* update pdu length */

		*olenptr = *pdulenptr + olen + 2; /* update entire message length */

#ifdef	J2_SNMPD_DEBUG
		if (snmpd_debug)
			snmpd_dump (&fsock, *olenptr + 2, odata);
#endif
		/* Okay, now send the SNMP data back out to whoever asked for it */
       	len = sizeof(fsock);
		j2sendto (Snmpd, odata, *olenptr + 2, 0, (char*)&fsock, len);
	}

	free_p(bp);
	close_s(Snmpd);
	Snmpd = -1;

    return 0;
}

/* 23Feb2012, Maiko, New function so that I can query for configuration info */
int dosnmp (int argc, char **argv, void *p)
{
	struct iface *ifp;

	int iface_cnt = 0;

	tprintf ("iface #  name\n");
    
	for (ifp = Ifaces; ifp != NULLIF; iface_cnt++, ifp = ifp->next)
		tprintf ("      %2d %s\n", iface_cnt, ifp->name);

	tprintf ("\n");

	return (0);
}

#endif	/* end of SNMPD */

