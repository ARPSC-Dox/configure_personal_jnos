#!/usr/bin/python -tt
#

import os, sys
import string
from gi.repository import Gtk, Gdk
import writesedscript, fillstores

class UI:
  def __init__(self, title="Configure Personal JNOS"):
   self.builder = Gtk.Builder()
   self.builder.add_from_file(os.path.join(os.getcwd(),
                              'Configure_Personal_JNOS.glade'))
   self.window = self.builder.get_object('dialog1')
   self.ether_adapt = self.builder.get_object('ether_adapter')
   self.close_button = self.builder.get_object('button_close')
   self.write_button = self.builder.get_object('button_write')

   # Initialize validity array
   self.valid = [ 0,0,0,0,0,0,0,0,0,0,0,1 ]

   # Fill county table with County number, code, name
   self.county_store =  self.builder.get_object('county_store')
   fillstores.fill_county_store( self.county_store )
   # Fill frequency table with valid packet frequencies
   self.frequency_store = self.builder.get_object('frequency_store')
   fillstores.fill_packet_frequencies(self.frequency_store)
 
   # Deal with the close and write buttons
   self.window.connect('delete-event', self.quit)
   self.close_button.connect('clicked', self.quit)
   self.write_button.connect('clicked', self.write_configs)
   self.write_button.set_sensitive(0)

   # Combo boxes
   # quad box filled in glade so selected item in glade
   self.quadbox = self.builder.get_object('combobox1')
   # Other boxes, initialize with a selection near the middle
   self.countybox = self.builder.get_object('combobox2')
   self.countybox.set_active(32)
   self.frequencybox = self.builder.get_object('combobox3')
   self.frequencybox.set_active(5)

   # Dull, get one entry field after another
   self.citybox   = self.builder.get_object('entry14')
   self.zipbox    = self.builder.get_object('entry16')
   self.callbox   = self.builder.get_object('entry11')
   self.ampripbox = self.builder.get_object('entry2')
   self.mynodebox = self.builder.get_object('entry1')
   self.hgnodebox = self.builder.get_object('entry3')
   self.hgipbox   = self.builder.get_object('entry4')
   self.gridbox   = self.builder.get_object('entry12')
   self.linbox    = self.builder.get_object('entry9')
   self.tunbox    = self.builder.get_object('entry10')
   self.macbox    = self.builder.get_object('entry8')
   self.devbox    = self.builder.get_object('entry6')
   self.ethbox    = self.builder.get_object('entry7')
   self.sysbox    = self.builder.get_object('entry17')
   self.spwbox    = self.builder.get_object('entry18')
   self.spwbo2    = self.builder.get_object('entry19')

   self.spwbox.connect('changed', self.check_passwords)
   self.spwbo2.connect('changed', self.check_passwords)
   self.callbox.connect('changed', self.upcase_field)
   self.sysbox.connect('changed', self.lowcase_field)
   self.mynodebox.connect('changed', self.lowcase_field)
   self.hgnodebox.connect('changed', self.lowcase_field)
   self.mynodebox.connect('focus-out-event', self.validate_node)
   self.hgnodebox.connect('focus-out-event', self.validate_hamgate_name)
   self.ampripbox.connect('focus-out-event', self.validate_mynode_ip)
   self.hgipbox.connect('focus-out-event', self.validate_hamgate_ip)
   self.linbox.connect('focus-out-event', self.validate_linux_ip)
   self.tunbox.connect('focus-out-event', self.validate_jnos_ip)

   # Initially set the color of passwords
   self.check_passwords()

   #self.get_ethers()

   # Make the window appear
   self.window.show()

  # Write out the sed script that will edit the config files
  def write_configs(self, *args):
   self.amprip  = self.ampripbox.get_text()
   self.mynode  = self.mynodebox.get_text()
   self.hgnode  = self.hgnodebox.get_text()
   self.hgip    = self.hgipbox.get_text()
   self.grid    = self.gridbox.get_text()
   self.linip   = self.linbox.get_text()
   self.tunip   = self.tunbox.get_text()
   self.mac     = self.macbox.get_text()
   self.device  = self.devbox.get_text()
   self.ether   = self.ethbox.get_text()
   self.sysoppw = self.spwbox.get_text()
   self.city    = self.citybox.get_text()
   self.zip     = self.zipbox.get_text()
   # sysop call must be lower case, call must be upper, and
   # lccall must be lower case
   sysopu       = self.sysbox.get_text()
   self.sysop   = string.lower( sysopu )
   callu        = self.callbox.get_text()
   self.call    = string.upper( callu )
   self.lccall  = string.lower( self.call )

   # Dummy in those that will be Combo Boxes
   aquad = self.quadbox.get_active()
   self.quad = str(aquad)
   acounty = self.countybox.get_active()
   #self.county = str(acounty)
   afreq = self.frequencybox.get_active()
   self.freq = str(afreq)

   # Get county number, abbrev and name from combobox
   ctree_iter = self.countybox.get_active_iter()
   if ctree_iter != None:
       cmodel = self.countybox.get_model()
       cid, ccode, countyn = cmodel[ctree_iter][:3]
       #print "Selected: ID=%s, name=%s" % (ccode, countyn)
   else:
       centry = self.countybox.get_child()
       print "Entered: %s" % centry.get_text()

   # Get frequency from combobox
   ftree_iter = self.frequencybox.get_active_iter()
   if ftree_iter != None:
       fmodel = self.frequencybox.get_model()
       fid, freq = fmodel[ftree_iter][:2]
       #print "Selected: ID=%d, name=%s" % (fid, freq)
   else:
       fentry = self.frequencybox.get_child()
       print "Entered: %s" % centry.get_text()

   # Get quad from combobox
   qtree_iter = self.quadbox.get_active_iter()
   if qtree_iter != None:
       qmodel = self.quadbox.get_model()
       quad, qid = qmodel[qtree_iter][:2]
       #print "Selected: ID=%d, name=%s" % (qid, quad)
   else:
       qentry = self.quadbox.get_child()
       print "Entered: %s" % qentry.get_text()

   writesedscript.write_sed(      quad,   self.city,   self.zip, 
                             self.lccall, self.amprip, self.call, 
                             self.mynode, self.hgnode, self.hgip, 
                                  freq,        countyn,self.grid,
                             self.linip,  self.tunip,  self.mac,
                             self.device, self.ether,  self.sysop,
                             self.sysoppw,     cid,         ccode )
   os.system('./writescript')

  # Check that passwords match before enabling write button
  def check_passwords(self, *args):
    sysoppw = self.spwbox.get_text()
    sysopp2 = self.spwbo2.get_text()
    if  sysoppw == sysopp2: 
      self.valid[11] = 0
      self.spwbox.override_color(Gtk.StateFlags.NORMAL, 
                                 Gdk.RGBA(0.0, 0.0, 0.0, 1.0))
      self.spwbo2.override_color(Gtk.StateFlags.NORMAL, 
                                 Gdk.RGBA(0.0, 0.0, 0.0, 1.0))

    else:
      self.valid[11] = 1
      self.spwbox.override_color(Gtk.StateFlags.NORMAL, 
                                 Gdk.RGBA(1.0, 0.0, 0.0, 1.0))
      self.spwbo2.override_color(Gtk.StateFlags.NORMAL, 
                                 Gdk.RGBA(1.0, 0.0, 0.0, 1.0))
    self.check_valid()

  # Check if valid
  def check_valid(self, *args):
    allvalid = 1;
    j = 0
    for i in self.valid:
      print "==>", j, i
      if i == 1:
        allvalid = 0
        print "Not all valid"
      j = j + 1
    if  allvalid == 1: 
      self.write_button.set_sensitive(1)
    else:
      self.write_button.set_sensitive(0)

  # Force a field to upper case
  def upcase_field(self, *args):
    widget = args[0]
    widget.set_text( widget.get_text().upper() )

  # Force a field to lower case
  def lowcase_field(self, *args):
    widget = args[0]
    widget.set_text( widget.get_text().lower() )

  # Initialize Ethernet address and MAC
  def get_ethers(self, *args):
    os.system('ifconfig >tempa')
    f = open('tempa','r')
    rawline = f.readline();
    # Get Linux IP and initialize linbox
    line = str(f.readline())
    parts = line.split(" ")
    ipaddr = str(parts[9])
    self.linbox.set_text(ipaddr)
    # Add 1 to initialize JNOS IP
    h = ipaddr.split(".")
    jnosip = str(h[0])+"."+str(h[1])+"."+str(h[2])+"."+str(int(h[3])+1)
    self.tunbox.set_text(jnosip)
    # Now initialize MAC address
    rawline = f.readline();
    line = str(f.readline())
    parts = line.split(" ")
    self.macbox.set_text(parts[9])
    f.close()
    os.system('rm -f tempa')

  # Validate an IP address
  def validate_ip( self, content ):
    e = content.split(".")
    i = 0
    for ele in e:
      i = i + 1
      #print i, ele
      if  int(ele) > 255:
        return 0
    if i != 4:
      return 0
    return 1

  # Validate My IP
  def validate_mynode_ip( self, *args ):
    w = args[0]
    content = w.get_text()
    res = self.validate_ip( content )
    if ( res ):
      w.override_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(0.0, 0.0, 0.0, 1.0))
      self.valid[1] = 0
      self.check_valid()
      return
    w.override_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(1.0, 0.0, 0.0, 1.0))
    self.valid[1] = 1
    self.check_valid()

  # Validate hamgate IP
  def validate_hamgate_ip( self, *args ):
    w = args[0]
    content = w.get_text()
    res = self.validate_ip( content )
    if ( res ):
      w.override_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(0.0, 0.0, 0.0, 1.0))
      self.valid[3] = 0
      self.check_valid()
      return
    w.override_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(1.0, 0.0, 0.0, 1.0))
    self.valid[3] = 1
    self.check_valid()

  # Validate Linux IP
  def validate_linux_ip( self, *args ):
    w = args[0]
    content = w.get_text()
    res = self.validate_ip( content )
    if ( res ):
      w.override_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(0.0, 0.0, 0.0, 1.0))
      self.valid[4] = 0
      self.check_valid()
      return
    w.override_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(1.0, 0.0, 0.0, 1.0))
    self.valid[4] = 1
    self.check_valid()

  # Validate JNOS IP
  def validate_jnos_ip( self, *args ):
    w = args[0]
    content = w.get_text()
    res = self.validate_ip( content )
    if ( res ):
      w.override_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(0.0, 0.0, 0.0, 1.0))
      self.valid[5] = 0
      self.check_valid()
      return
    w.override_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(1.0, 0.0, 0.0, 1.0))
    self.valid[5] = 1
    self.check_valid()

  # Validate an amprnet node name
  def validate_amprname( self, content ):
    e = content.split(".")
    i = 0
    for ele in e:
      i = i + 1
      #print i, ele
    if i<3:
      return 0
    if e[i-1] != 'org':
      return 0
    if e[i-2] != 'ampr':
      return 0
    return 1

  # Validate my node name
  def validate_node(self, *args):
    w = args[0]
    content = w.get_text()
    res = self.validate_amprname( content )
    if ( res ):
      w.override_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(0.0, 0.0, 0.0, 1.0))
      self.valid[0] = 0
      self.check_valid()
      return
    w.override_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(1.0, 0.0, 0.0, 1.0))
    self.valid[0] = 1
    self.check_valid()
 
  # Validate a hamgate name
  def validate_hamgate_name(self, *args):
    w = args[0]
    content = w.get_text()
    res = self.validate_amprname( content )
    if ( res ):
      w.override_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(0.0, 0.0, 0.0, 1.0))
      self.valid[2] = 0
      self.check_valid()
      return
    w.override_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(1.0, 0.0, 0.0, 1.0))
    self.valid[2] = 1
    self.check_valid()
    

  # Close button
  def quit(self, *args):
      Gtk.main_quit()

##########################################################
#  HEY - we actually *DO* have a mainline!
##########################################################

if __name__ == '__main__':
  ui = UI()
  Gtk.main()

