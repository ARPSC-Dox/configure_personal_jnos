
rewrite <address>

        Will show the result of the mail rewrite process, that is,
        how <address> is changed based on the contents of Rewritefile
        (default location is /spool/rewrite).  The result of rewriting
        determines whether an incoming message is stored in an area (and,
        which area receives the message), or whether it is stored into
        /spool/mqueue for handling by SMTP.  The rewrite command is useful
        for testing modifications to Rewritefile.

        Example:  rewrite n8fow@n8fow.ampr.org
        shows the mail rewriting that will be done for mail addressed to
        n8fow@n8fow.ampr.org.

REWRITE FILE
        The Rewritefile is used to perform a one-to-one mapping
        between a message's destination (To:) addresses as received
        by JNOS, and the destination addresses as actually used by JNOS.
        Each record within the rewrite file comprises a single line,
        containing either two or three fields separated by spaces. The
        first field is the template field; if a destination address
        matches the template, it is replaced by the second field after
        variable substitution.  The rewrite process terminates after the
        first template match, with Jnos using the resultant value as the
        new destination address, unless there is an optional third field
        which contains the single letter "r".  In this case, JNOS rescans
        the rewrite file from the beginning, attempting to match the new
        destination address with one of the templates.  If no template
        matches the destination, it is used unchanged as the result.

        If the address resulting from rewrite contains an "@" it is sent
        via smtp.  Otherwise it should be the name of an area.  There is
        one special case: "refuse" as the destination causes the message
        to be rejected.

        A template contains a combination of verbatim ascii text, and
        special characters "?*+\".  To treat a special character as an
        ordinary character, precede it by '\'.  The '?' character matches
        any single character, '*' matches any number of consecutive
        characters (including zero), and '+' matches a sequence of one or
        more characters.  In the second field, the character "$", followed
        by a single digit in the range 1 to 9, represents the string that
        matched the respective '*' or '+' in the template.  For example,
                tcpip@* tcp
                *@tcpip tcp

        rewrites all addresses beginning or ending with "tcpip" to the
        tcp area.  The two-character sequence, $H, in the second field
        is replaced by the hostname of the system, as set by the hostname
        command.

            
SAMPLE REWRITE FILE (edit to suit your call and neighbors):

# Sample rewrite file for K5ARH pbbs, also known as w5ddl.ampr.org
#
# Special destinations: refuse  => send NO (ax.25) or Bad host (smtp)
# (a.k.a. "areas")      nts*    => mailbox writable by all (ie, kill cmd works)
#                                       Also, ST used in forwarding.
#                       help    => never flag as already read, so L shows all
# take x%y@me and reprocess as x@y
*%*@w5ddl* $1@$2 r
*%*@localhost* $1@$2 r
# For dual-id'ed BBSes, treat @one as @other:
*@k5arh* $1@w5ddl r
#
# Next, pass real FQDN's unchanged
*@*.edu $1@$2.edu
*@*.com $1@$2.com
*@*.gov $1@$2.gov
*@*.org $1@$2.org
*@*.net $1@$2.net
*@*.mil $1@$2.mil

#
# Now addresses we discard with impunity (junk, useless stuff from gatewayed systems)
#eg, astro@* refuse
#eg, *@dist9 refuse
dx@ww refuse
#

# Handle local sysop, and sysop bulls
sysop k5arh
sysop@w5ddl* k5arh
sysop@allla allla
sysop@* sysop
*@sysop sysop
#

# Now pass specific bulletins on to our areas
# NOTE: It is important that the lines below are kept in that order
# (Ie TO sorting FIRST, then AT sorting !!)
# Otherwize something like 'amsat@allusa' will end up in the 'allusa' area
# instead of the 'amsat' area where I prefer it.
tcpip@* tcpip
wanted@* wanted
want@* wanted
need@* wanted
wp@* btrbbs
sale@* sale
4sale@* sale
trade@* sale
swap@* sale
dx@* dx
races@* races
fcc@* fcc
amsat@* amsat
arrl@* arrl
ares@* ares
nasa@* nasa
gulf@* gulf
keps@* keps
larc@* larc
kd5sl@* btrbbs
#
*@gulf gulf
*@nasa nasa
*@amsat amsat
*@ares* ares
*@arrl arrl
*@arl arrl
*@la allla

*@allla allla
*@allus* allus
*@allbbs* allus
*@us* allus
*@usa allus
*@franca franca
*@ww ww
laoep@* laoep
*@laoep* laoep
#

# LARC is local Lafayette distribution
*@larc larc
*@test test
#

# Anything left @w5ddl is private mail to my users
*@w5ddl* $1
w5ddl@* k5arh

#
# Handle local hosts I forward to:
*@n5knx* n5knx
*@wu3v* wu3v
# Now BBSes we pass north/east/westward
*@wb5bke* bkebbs
*@kb5ogn* ognbbs
*@ka5nmn* ognbbs
*@k4ry* k4ry
*@ka4pkb* ka4pkb
*@w5ksi* norbbs
*@kb4gbs* norbbs
*@kk4cq* norbbs
*@n5ssy* norbbs
*@w4iax* norbbs
*@n5uxt* norbbs
*@wd5ghw* btrbbs
*@kd5sl* btrbbs
*@ae5v* aexbbs
*@ke5l* aexbbs
*@wg5w* aexbbs
*@kb5bfv* lchbbs
*@n2ktq* lchbbs
*@wb5txn* lchbbs
*@kb5tbb* cnbbbs
*@kb5vjy* aexbbs
*@wa4imz* cnbbbs
*@ka5kth* cnbbbs
*@wb5fro* cnbbbs
*@f6cnb* cnbbbs
*@w0gvt* cnbbbs
ka3zyp@* cnbbbs
#
# Handle frequent mistakes or special cases or some other reason I now forget:
71*@* aexbbs
kb5bfv lchbbs
kb5bfv@* lchbbs
ae5v@* aexbbs
kc5gwh@* bkebbs
kc5gwh bkebbs
n5ssy norbbs
ka5nmn@* ognbbs
ka5ydj@* norbbs
n5ssy@* norbbs
wb5vtn@* norbbs
n5eqo@* norbbs
# <<<others here>>>
# NTS local, in-state, outside
*@705* ntslocal
*@71* aexbbs
*@706* lchbbs
*@70* btrbbs
*@ntstx* cnbbbs
*@nts* nts
# Now state-specific forwarding (if any)
*@*.tx* cnbbbs

#
# Continents via HF at west f6cnb
*@*.eu cnbbbs
*@*.oc cnbbbs
*@*.noam cnbbbs
*@*.na cnbbbs
*@*.usa cnbbbs
*@*.as cnbbbs
*@*.af cnbbbs
*@*.sa cnbbbs

#
# Anything else means we must add more, above
*@* check

#
# Try to handle addressing mistakes by mbox users!
*/* check
*\* check
*&* check
*.* check
