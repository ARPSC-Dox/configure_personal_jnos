
tcp <subcommand>

     These commands are used for the Transmission Control Protocol
service. All TCP parameters are configurable per interface.  Commands
of the form 'tcp <command>' set the default or global values.  Use
the 'ifconfig <iface> tcp <command>' form to set or show the interface-
specific values.

     To set the system default TCP parameters, you must do so BEFORE
attaching interfaces.  After attachment, you must use the 'ifconfig
<iface> tcp' command form to show or change values for that interface.
THIS IS A CHANGE FROM THE BEHAVIOUR EXHIBITED PRIOR TO JNOS VERSION
1.10X16, RELEASED 08FEB94.


     Notes:

     Attempting outgoing connections to addresses without an
     existing route results in Error number 219.


    tcp access <permit|deny|delete> <ipaddr[/bits]|all> [loport [hiport]]

     Display or set tcp access controls, which determine which TCP services
     (ports) are accessible to which IP addresses.  If no tcp access commands
     are issued, the default behaviour is to permit all hosts to access all
     ports.  But once a TCP access command is entered, all other ports and
     addresses are denied unless specifically permitted by subsequent tcp
     access commands.

     This subcommand adds or deletes an access control entry maintained
     in an internal table.  Incoming tcp packets are compared with the table
     entries, in the order that they were added, to determine if access will
     be granted. Access is granted only if an entry with matching ipaddr or
     range, and ports, is found with "permit" set before either a match with
     "deny" set if found, or the end of the table is reached.
     The optional /bits suffix to the ipaddr specifies how many leading
     bits in the ipaddr are to be considered significant in the address
     comparisons.  If not specified, 32 bits (i.e., full significance)
     is assumed. All addresses can be specified by "all". Loport and
     hiport specify the port or range of TCP ports for which the access
     control command applies. If "all" is given as the loport, or if no
     port range is specified, all ports are assumed, ie, 1 to 65534.

     "tcp access" will display the table of current access control entries.

     Access commands should be entered from the most specific ipaddr to the
     least specific, since the first match (permit or deny) encountered
     in the internal table is definitive.

     #Example:
         #Allow a specific AMPRnet host SMTP access
         tcp access permit 44.76.1.199 25
         #but deny all other services to him
         tcp access deny 44.76.1.199
         #Allow all other AMPRnet hosts full access to TCP services
         tcp access permit 44.76.1/24 all

         #Allow a specific subnet access to ports 1 through 25,
         #which includes echo, discard, ftp, telnet, and smtp.
         tcp access permit 23.1.46/24 1 25
         #Note that all other hosts not matched above, are denied access


    tcp blimit [<value>]                           Default: 31

     Display or set the default tcp retransmission backoff limit.
     Normally each successive tcp retransmission is delayed a time
     value that increases exponentially or linearly.  The backoff
     limit <value> serves to set the maximum backoff delay allowed.
     See also tcp timertype and tcp maxwait.
     

    tcp clean

     Reset all tcp connections that are in a "FIN wait 2" state.  This
     is useful to release memory resources held by Jnos for connections
     that were not properly closed.


    tcp irtt [<milliseconds>]

     Display or set the initial round trip time estimate, in
     milliseconds, to be used  for new TCP connections until they can
     measure and adapt to the actual value.  The default is 5000
     milliseconds (5 seconds).  Increasing irtt when operating over
     slow  channels will avoid the flurry of re-transmissions that
     would otherwise occur as the smoothed estimate settles down at
     the correct value.  Note that this command should be given before
     servers are started in order for it to have effect on incoming
     connections.
     
     TCP also keeps a cache of measured round trip times and mean
     deviations (MDEV) for current and recent destinations.  Whenever
     a new TCP connection is opened, the system first looks in this
     cache.  If the destination is found, the cached IRTT and MDEV
     values are used. If not, the default IRTT value mentioned above
     is used, along with a MDEV of 0.  This feature is fully
     automatic, and it can improve performance greatly when a series
     of connections are opened and closed to a given destination (e.g.
     a series of FTP  file transfers or directory listings).


    tcp kick <tcb_addr>

     If there is unacknowledged data on the send queue of the
     specified TCB, this command forces an immediate retransmission.
     <tcb addr> can be found with the 'tcp status' command.
     

    tcp maxwait [<msec>]

     Set or show the maximum time for retry timeout in milliseconds.
     Default = 0, no maximum.
     
     
    tcp mss [<size>]

     Display or set the TCP Maximum Segment Size in bytes that
     will be sent on all outgoing TCP connect request (SYN segments).
     This tells the remote end the size of the largest segment
     (packet) it may send.  Changing MSS affects only future
     connections; existing connections are unaffected.  See also the
     section 'Of PACLEN, MTU, MSS, and More' in the JNOS40
     Configuration Manual.
     

    tcp reset <tcb_addr>

     Deletes the TCP control block at the specified address.
     

    tcp retries [<num>]

     Display or set the number of retries before a tcp connection
     will be reset. Default is 10. This is useful to eliminate idle
     connections that have not been properly shut down. If set to zero,
     there is no maximum, i.e. a connection will never retry out.
     

    tcp rtt <tcb_addr> <milliseconds>

     Replaces the automatically computed round trip time in the
     specified TCB with the rtt in milliseconds.  This command is
     useful to speed up recovery from a series of lost packets since
     it provides a manual bypass  around the normal backoff
     retransmission timing mechanisms.
     

    tcp status [<tcb_addr> | all]

     Without arguments, displays several TCP-level statistics, plus a
     summary  of all  existing  TCP connections, including TCB
     address, send and receive queue sizes, local and remote sockets,
     and connection state. If <tcb addr> is  specified,  a more
     detailed dump of the specified TCB is generated, including send
     and receive sequence numbers and timer information.  If "all" is
     given, the summary will also include TCBs in a listening state
     (awaiting a connection).  In this case, a (S) will indicate that
     a server process is to be spawned when a connection occurs.


    tcp syndata [yes | NO]

     Display or set the tcp syn + data piggybacking flag. Some
     tcp systems cannot handle syn + data together.
     

    tcp timertype [linear | exponential]

     Display the current setting or set the timer type backoff
     algorithm.  Default is exponential.
     

    tcp trace [yes | NO]

     Display or set the tcp trace flag on or off.
     

    tcp window [<size>]

     Displays or sets the default receive window size in bytes to
     be used  by  TCP when creating new connections. Existing
     connections are unaffected.
     


