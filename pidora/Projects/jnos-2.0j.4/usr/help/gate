gate <subcommands>

   The gate command, available when Jnos is compiled with TCPGATE #define'd,
   manages the tcpgate server, which accepts connects to certain tcp ports,
   and logically redirects them to another host and/or tcp port.  Thus it
   is only one half of a proxy server!

   When invoked without arguments, gate displays the current state of any
   active connections through the tcpgate code. On the left is the
   originating address and port, on the right is the destination address
   and port, as determined by the 'start gate...' command, and in the centre
   is the port that was used by tcpgate to trap the connection.

gate status
   displays the manner in which incoming connections are mapped to what
   outgoing ones. It also displays the number of client connections
   allowed and the inactivity timeout.

gate maxcli
   determines how may clients may connect through the tcpgate at any one
   time.

gate tdisc
   determines the inactivity timeout (in seconds) as measured by data
   SENT to the client. A value of zero disables this feature.

start gate port hostname [port]
   this is used to start up the tcpgate server on a particular port. The
   server lisens for connections on that port and accepts them. It then
   makes a connection to the nominated hostname (with optionally a new
   port number - otherwise the same port number as is listened for it
   used). The 'gate status' command may be used to determine what ports
   have already been configured. This command generates a failure if the
   port number is already in use.

