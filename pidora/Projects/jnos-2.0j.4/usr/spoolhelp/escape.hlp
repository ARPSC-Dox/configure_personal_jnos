     E[scape] [<char>|<integer>|off|on]

     The  escape  command, when  entered by  itself, will display the
     character that  is currently set  as the  escape character, and
     whether escape processing is enabled.  This character is what
     will be  used if you  want to  exit from the current  session.
     For  instance,  if you have  started a "chat"  session, and you
     don't get  any response from the operator after waiting a few
     minutes,  you can  enter  the  escape  character, followed by a
     <RETURN>  or  <ENTER>, and the  session will be terminated.  You
     will then be returned to the MBOX prompt.

     The escape  character may be  changed to one  of your preference
     by entering "escape"  followed  by a  <SPACE> and  the character
     that will  become  the  new  escape  character.  This  must be a
     single typed character (the <CTRL> key may be used in addition).
     Alternatively, an <integer> corresponding to the decimal code for
     the escape character may be specified.

     Escape processing can be enabled or disabled by specifying "on" or
     "off" as the only argument.

EXAMPLES
     escape ^Z          (the ASCII character <CTRL>Z)
     escape X           (the character "X" is the new escape)
     esc 120            (the character "x" is the new escape)
     escape off         (suspend escape char processing)
     escape on          (resume escape char processing)
