
expire <subcommand>

     The 'expire' command is used to invoke the process which deletes
old BBS messages.  The mailboxes or newsgroups to be expired, and the
age (in days) after which a message may be deleted, are specified in
the Expirefile (defaults to /spool/expire.dat).


    expire [now | interval]

     Begin the expire process immediately, if <now> is specified, or
     every <interval> hours in the future (but no immediate expire is
     done). If no arguments are provided, the current expire interval
     is displayed.

         Example:  To run expire at 04:30 each morning, place these
         commands into your autoexec.nos file
                   at 0430 "expire 24"
                   at 0431 "expire now"


Format of Expirefile:

#comment line
mbox_name  number_of_days_to_retain_messages
mbox_path  number_of_days_to_retain_messages
!news.group.name  number_of_days_to_retain_articles

If the number of days is omitted, 21 is used.  Also, if newsgroups
are specified, the associated History file is expired as well, but
the number of days used is the maximum of all the days provided for
newsgroups in the Expirefile.

     Example:  allusa 7
               amsat 10
               !swap 10
               !rec.radio 7


See Also:  oldbid
