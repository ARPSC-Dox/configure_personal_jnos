remote [-s <syskey>] [-g <gwkey>]
remote [-p <port>] [-k <key>] [-a <kickaddr>] <host> kick | exit | reset
remote [-p <port>] [-k <key>] [-r addr/#bits] <host> add | drop

        The remote command can be invoked three ways.
        First, to set a key phrase used by the remote server process to
        validate commands sent to it.  Second, to send a UDP packet to a
        specified host commanding it to exit NOS, reset the processor,
        or force a retransmission on TCP connections.  Third, to send a
        UDP packet to a specified host commanding it to either add or
        drop an encapped route to a specified host or subnet via the
        sending system's IP address, that is, register ourselves as a
        gateway for a specified host or subnet for a finite period.

        For the second and third forms of the remote command to be
        accepted, the remote system must be running the remote server.
        Also, the port number specified in the remote command must match
        the port number given when the server was started on the remote
        system.  Further, if the remote system established a key phrase,
        then that phrase must also be provided via the -k option, so the
        remote system can check that it matches.

        If the port numbers or keywords do not match, or if the remote
        server is not running on the target system, the command packet
        is ignored.

        Even if the command is accepted there is no acknowledgement.

        The 'kick' subcommand forces a retransmission timeout on all TCP
        connections that the remote node may have with the local node.

        If the '-a' option is used, connections to the specified host are
        kicked instead.  No key is required when using the 'kick'
        subcommand.

        The 'exit' and 'reset' subcommands are mainly useful for
        restarting NOS on a remote unattended system after the
        configuration file has been updated.  The remote system should
        invoke NOS automatically upon booting, preferably in an infinite
        loop.

        The add or drop subcommands allow a Jnos system with a dynamically-
        assigned IP address, to register as a gateway with a cooperating
        system having a known IP address.  The duration of the route
        thus established is controlled by the remote system, typically
        15 minutes.  Since UDP packets are not guaranteed to be delivered,
        it might be wise to send remote commands more frequently than the
        timeout.  See the at command, well suited to this purpose.

        remote -s [<key>]
          The 'exit' and 'reset' subcommands of 'remote' require a
          password.  The password is set on a given system with the '-s'
          option, and it is specified in a command to a remote system with
          the '-k' option.

          If no password is set with the '-s' option, then the 'exit' and
          'reset' subcommands are disabled.

        remote -g [<gwkey>]
          The 'add' and 'drop' subcommands of remote may require a
          password.  The password is set on a given system with the '-g'
          option, and it is specified in a command to a remote system with
          the '-k' option.

          Note that 'remote' is an experimental feature in JNOS; it is not
          yet supported by any other non-KA9Q-derived TCP/IP implementations,
          although a version of the remote command for BSD Unix exists.



