
start <servers...>

     Start the specified server.  Do a 'start ?' for a list of
     available servers (this won't be displayed to remote sysops).  

     Servers often take an argument which specifies an alternate
     port number from which to listen.  Example:
            start callbook [port#]               Default port: 1235
     Default port numbers are listed in socket.h.


    Exceptions:

     start tip <iface> [modem | terminal] [timeout_secs]

      starts a mailbox server which listens for connections on <iface>.
      If "modem" is used, CD (carrier detect) must be asserted before a
      connection is recognized.  A <timeout> value specifies how many
      seconds of inactivity must occur before a disconnect is initiated.


      A tip about using the tip server to provide async dialup access to
      Jnos:  the following commands demonstrate one way to set it up.

      # source this file to configure COM1 for dialup access to JNOS
      attach asy 0x3f8 4 ax25 dialup 2048 256 9600
      #
      ifconfig dialup descrip "dialup access"
      param dialup up
      # Send any desired config cmds here, eg, answer phone on 1st ring
      comm dialup "atz e0 s0=1"
      pause 2
      #start tip dialup terminal 360
      start tip dialup modem 360
      # If the modem obeys DTR (must be asserted to answer the phone)
      # the modem might be permed with S0=1, and then the answering could
      # be controlled by param dialup up, param dialup down, commands
      # possibly done daily by a repeating at command...


     start http [port#] [drive] [rootdir]

      starts an http server.  The default port is 80, the default disk drive
      is C, and the default rootdir (for html file access) is /wwwroot.
      See the http helpfile for more information, especially in the notes
      section.
