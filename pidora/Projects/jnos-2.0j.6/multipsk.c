/*
 * Support for MULTIPSK (tcp/ip control) as a digital modem
 * Designed and Coded (February 2009) by Maiko Langelaar, VE4KLM
 * First official prototype source checked in March 01, 2009 
 *
 * (C)opyright 2009 Maiko Langelaar, VE4KLM
 *
 * For Amateur Radio use only (please) !
 */

#include "global.h"

#ifdef	MULTIPSK

#include "mbuf.h"
#include "iface.h"
#include "slip.h"
#include "trace.h"
#include "pktdrvr.h"
#include "devparam.h"

typedef struct mpskcparm {
	char *hostname;
	int portnum;
} IFPMPSK;

/* 27Feb2009, Maiko, Use an existing function from slip.c module  */
extern struct mbuf *slip_decode (register struct slip *sp, char c);

/* 28Feb2009, Maiko, Use an existing function from slip.c module  */
extern struct mbuf *slip_encode (struct mbuf *bp, int usecrc);

static int psock = -1;

static int connectmpsk (IFPMPSK *ifpp)
{
    struct sockaddr_in fsocket;

    int s = -1;
  
	if ((fsocket.sin_addr.s_addr = resolve (ifpp->hostname)) == 0L)
	{
		log (-1, "multipsk - host (%s) not found", ifpp->hostname);
        return -1;
    }
  
    fsocket.sin_family = AF_INET;
    fsocket.sin_port = ifpp->portnum;
 
	if ((s = j2socket (AF_INET, SOCK_STREAM, 0)) == -1)
	{
		log (-1, "multipsk - no socket");
        return -1;
	}

	sockmode (s, SOCK_BINARY);
  
	if (j2connect (s, (char*)&fsocket, SOCKSIZE) == -1)
	{
		log (-1, "multipsk - connect failed, errno %d", errno);
        return -1;
	}
  
	log (s, "multipsk [%s] - connected", ifpp->hostname);

	return s;
}

int mpsk_send (struct iface *iface, struct mbuf *bp)
{
	static char *iobuf = NULL;
	static int iomtu = -1;
	struct mbuf *tbp;
	int len;

	if (iomtu == -1 || iface->mtu > iomtu)
	{
		if (iomtu != -1)
			free (iobuf);

		iomtu = iface->mtu;
		iobuf = mallocw (iomtu);
	}

	if (psock != -1)	/* tcp/ip socket handle */
	{
		len = len_p (bp);

		/* this should never happen, but just to be safe !!! */
		if (len > iomtu)
			log (-1, "mpsk_send - packet (%d) larger then mtu", len);

		else if (dup_p (&tbp, bp, 0, len) != len)
			log (-1, "mpsk_send - dup_p failed");

		else
		{
			char *ptr, *ptr2, *iobuf2 = mallocw (len * 2);
			int cnt = 0;

			(void)pullup (&tbp, iobuf, len);

			ptr = iobuf2;
			ptr2 = iobuf;
		
			while (cnt < len)
			{
				*ptr++ = 22;
				*ptr++ = *ptr2++;
				cnt++;
			}	

			if (j2send (psock, iobuf2, 2*len, 0) < 1)
				log (-1, "mpsk_send - write errno %d", errno);

			free_p (tbp);
		}

	    free_p (bp);
	}

    return 0;
}

int mpsk_raw (struct iface *iface, struct mbuf *bp)
{
    struct mbuf *bp2, *bp3;

	struct slip *sp = &Slip[iface->xdev];
  
    bp2 = pushdown (bp, 1);
    bp2->data[0] = PARAM_DATA;

	dump (iface, IF_TRACE_OUT, sp->type, bp2);

    iface->rawsndcnt++;
    iface->lastsent = secclock();
  
    if ((bp3 = slip_encode (bp2, sp->usecrc)) == NULLBUF)
        return -1;

	mpsk_send (iface, bp3);

    return 0;
}

void mpsk_rx (int xdev, void *p1, void *p2)
{
	struct iface *ifp = (struct iface*)p1;

	IFPMPSK *ifpp = (IFPMPSK*)p2;

	struct slip *sp;
    struct mbuf *bp;
	int c;

    sp = &Slip[ifp->xdev];

	log (-1, "multipsk listener [%s:%d]", ifpp->hostname, ifpp->portnum);

	while (1)
	{
		/* Connect to the multipsk tcp/ip server, retry every minute */

		if (psock == -1)
		{
			while ((psock = connectmpsk (ifpp)) == -1)
				j2pause (60000);
		}

		/* Handle incoming data stream */

		bp = NULL;

		while ((c = recvchar (psock)) != -1)
		{
			if (c == 0x16)	/* Patrick preceeds KISS bytes with CHR(22) */
			{
				if ((c = recvchar (psock)) == -1)
					break;

				/*
				 * 27Feb2009, Maiko, Use existing functions, no point
				 * reinventing the wheel, it's there already. Just need
				 * to make it non-static in slip.c, so that we can call
				 * it from here. This should work nicely, simple code.
				 */

				if ((bp = slip_decode (sp, (char)c)) == NULLBUF)
					continue;   /* More to come */

				if (net_route (sp->iface, sp->type, bp) != 0)
				{
					free_p (bp);
					bp = NULL;
				}

			}
			else if (c == 0x1d)	/* Patrick preceeds TEXT bytes with CHR(29) */
			{
				if ((c = recvchar (psock)) == -1)
					break;

				/* Just ignore the text data for now */
			}
		}

		log (-1, "multipsk disconnected");
		close_s (psock);
		psock = -1;
	}
}

int mpsk_attach (int argc, char *argv[], void *p)
{
	struct iface *ifp;
	struct slip *sp;
	IFPMPSK *ifpp;
	int xdev;
 
	if (if_lookup (argv[1]) != NULLIF)
	{
		tprintf (Existingiface, argv[4]);
		return -1;
	}

	/* Create structure of Multipsk connection  parameters */

	ifpp = (IFPMPSK*)callocw (1, sizeof(IFPMPSK));

	ifpp->hostname = j2strdup (argv[2]);
	ifpp->portnum = atoi (argv[3]);

	/* Create interface structure and fill in details */

	ifp = (struct iface*)callocw (1, sizeof(struct iface));

	ifp->addr = Ip_addr;
	ifp->name = j2strdup (argv[1]);
	ifp->mtu = 256;
	ifp->dev = 0;
	ifp->stop = NULL;

	if (ifp->hwaddr == NULLCHAR)
		ifp->hwaddr = mallocw (AXALEN);

	memcpy (ifp->hwaddr, Mycall, AXALEN);

	setencap (ifp, "AX25");

	ifp->ioctl = NULL;
	ifp->raw = mpsk_raw;
	ifp->show = NULL;
	ifp->flags = 0;

	/*
	 * 28Feb2009, Maiko, Crap, it looks like the Slip[] and xdev and dev are
	 * so interlocked into the trace (dump) routines, that dump will actually
	 * crash (and probably other stuff) if I just calloc a SLIP structure, as
	 * I tried to do before. Looks like I'll have to stick with looping thru
	 * the Slip[] array for an empty slot, until I figure out another way to
	 * do this. It works ! BUT I don't like how it's tied into everything.
	 */
	for (xdev = 0; xdev < SLIP_MAX; xdev++)
	{
		sp = &Slip[xdev];
		if (sp->iface == NULLIF)
			break;
	}
	if (xdev >= SLIP_MAX)
	{
		j2tputs ("Too many slip devices\n");
		return -1;
	}

	ifp->xdev = xdev;
	sp->iface = ifp;
	sp->kiss[ifp->port] = ifp;
	sp->type = CL_KISS;
	sp->polled = 0;
	sp->usecrc = 0;

	/* Link in the interface - important part !!! */
	ifp->next = Ifaces;
	Ifaces = ifp;

	/*
	 * Create a listener process
	 *
	 * The connect to the multipsk tcp/ip server is inside the
	 * listener process. If we do it here, JNOS will hang on the
	 * attach command - remember 'attach' must always return.
	 */
	ifp->rxproc = newproc ("mpsk_rx", 1024, mpsk_rx,
			0, (void*)ifp, (void*)ifpp, 0);

	return 0;
}

#endif	/* End of MULTIPSK */
