/*
 * JNOS 2.0
 *
 * $Id: sort.c,v 1.1 2012/03/20 16:51:27 ve4klm Exp $
 *
 * 02Feb2012, Maiko (VE4KLM), From Lantz TNOS code - need for WPAGES feature
 */

#include "global.h"

#ifdef	WPAGES

#include "commands.h"
#include "unix.h"
#include "proc.h"

#include <time.h>

/*
static char rcsid[] = "$Id: sort.c,v 1.1 2012/03/20 16:51:27 ve4klm Exp $";
 */

#define ENDING ((size_t) -1)

int expired;

struct entries {
	char *name;
	long theindex;
	size_t nextindex;
};

static struct entries *e = ((struct entries *)0);
static int count;
static size_t current;
static char name[16];
static int result, same;
static size_t theindex;
static long currentindex;
static int sortdups = 0, wassorted = 0;
static size_t sorthead;
static size_t lastindex;

static struct entries *temp = (struct entries *) 0;
static struct entries *temp2 = (struct entries *) 0;

static void search (void);
static void insert (void);
static void makesortname (char *buf, const char *origname);

static void search ()
{
	same = 0;
	lastindex = ENDING;
	for (theindex = sorthead; theindex != ENDING; theindex = e[theindex].nextindex)		{
		result = strnicmp (name, e[theindex].name, strlen(name));
		if (!result)	{
			same = 1;
			sortdups++;
			return;
		}
		if (result < 0)
			return;
		lastindex = theindex;
	}
	return;
}

static void insert ()
{
	temp = &e[current];
	if (lastindex == ENDING)	{	/* at head of list */
		temp->nextindex = sorthead;
		sorthead = current;
	} else 	{	/* in the midst or tail of file */
		temp2 = &e[lastindex];
		if (theindex)		/* in midst of list */
			temp->nextindex = temp2->nextindex;
		temp2->nextindex = current;
		if (!wassorted && !same && !theindex)
			wassorted = 1;
	}

	strcpy (temp->name, name);

	if (!same)
		temp->theindex = currentindex;
	else
		temp->theindex = -1;
	current++;
	count++;
}

static void makesortname (char *buf, const char *origname)
{
	char *cp;

	strcpy (buf, origname);
	cp = strchr (buf, '.');
	if (!cp)
		cp = &buf[strlen(buf)];
	strcpy (cp, ".srt");
}

void sortit (const char *fname, int entrysize,
		int searchsize, int strsize, time_t date)
{
	char *cp;
	unsigned k;
	FILE *fp, *out;
	char buf[128];
	long size0, size;
	time_t now, stamptime;
	int newnum = 0;

	if ((fp = fopen (fname, "rt")) == 0)
		return;

	makesortname (buf, fname);

	if ((out = fopen (buf, "wt")) == 0)
	{
		fclose (fp);
		return;
	}
	size0 = (long) filelength(fileno(fp));
	size = size0 / (long) entrysize;
	size += 1;

	log(-1,"Sort '%s' - %ld Entries originally", fname, size - 1);

	e = (struct entries *) mallocw ((unsigned)((unsigned long)size * sizeof(struct entries)));

	for (current = 0; current < (size_t) size; current++)	{
		e[current].name = (char *)mallocw ((unsigned)(searchsize + 1));
		e[current].name[0] = 0;
		e[current].nextindex = ENDING;
		e[current].theindex = -1;
		pwait (NULL);
	}

	sorthead = ENDING;
	current = count = 0;
	expired = sortdups = wassorted = 0;
	now = time(&now);
	while (!feof (fp))
	{
		pwait (NULL);
		currentindex = ftell(fp);
		if ((long)current == size)
			break;

		fgets (buf, entrysize, fp);

		if (feof (fp))
			continue;

		pwait (NULL);

		if (*buf > ' ' && ((int) strlen(buf) > (entrysize - 2))
			&& (*buf != '#'))
		{
			strncpy (name, buf, (unsigned)searchsize);
			name[searchsize] = 0;
			if ((cp = strpbrk (name, ".@ \t")) != 0)
				*cp = 0;
			search();
			pwait (NULL);
			insert ();
			pwait (NULL);
		} else	{	/* skip it! */
			current++;
			count++;
			continue;
		}
		/* now check for expired date stamp, if used */
		if (date)	{
			cp = strchr (buf, ' ');
			if (!cp)	{
				stamptime = 0;
			} else {
				cp = skipwhite (cp);
				stamptime = atol(cp);
			}
			if ((stamptime == 0) || (now - stamptime >= date))	{
				expired++;
				e[current].theindex = -2;
			}
		}
	}
	fflush (stdout);
	if (expired || wassorted || sortdups)		{
		for (theindex = sorthead,k=0; k < (unsigned)count && theindex != ENDING; k++,theindex = temp->nextindex)	{
			temp = &e[theindex];
			if (temp->theindex >= 0)	{
				pwait (NULL);
				fseek (fp, (long) temp->theindex, 0);
				fgets (buf, entrysize, fp);

				if (strsize)	{

					/* we now re-format it, just in case it is bad */
					if ((cp = strpbrk (buf, " \t")) == 0)
						continue;

					*cp++ = 0;
					cp = skipwhite (cp);
					stamptime = atol(cp);
					buf[strsize] = 0;	/* just in case */
					cp = buf;
					while (*cp)	{
						if (*cp & 0x80)	{
							*cp = 0;
							break;
						}
						cp++;
					}
					if (stamptime && *buf)
				                fprintf(out,"%-*s %-14ld\n",strsize,buf,stamptime);
			        } else
					fputs (buf, out);
				newnum++;
			}
		}
	}
	log(-1,"Sort '%s' - %d Entries at end", fname, newnum);
	fclose (fp);
	fclose (out);
	for (current = 0; (long)current < size; current++)
		free(e[current].name);
	if (e != ((struct entries *)0))	{
		free (e);
		e = ((struct entries *)0);
	}

	makesortname (buf, fname);
	if (expired || wassorted || sortdups)	{
		(void) remove (fname);
		(void) rename (buf, fname);
	} else
		(void) remove (buf);
}

#endif	/* end of WPAGES */
