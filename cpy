#!/bin/sh
DIR="Projects/Amateur_Radio/jnos/configJNOS/"
F1="Configure_Personal_JNOS Configure_Personal_JNOS.glade writesedscript.py writescript"
F2="jnos autoexec.nos ftpusers nos.cfg popusers write_ether startnos iptables-config access.www channel.dat convmotd.txt ftpmotd.txt forward.bbs motd.txt names.dat rewrite users.dat info.hlp makeuser getty@tty6.service"
mkdir /usr/share/jnos
cd /usr/share/jnos
for F in $F1; do
  scp jjmcd@192.68.0.17:${DIR}${F} .
done
mkdir templates
for F in $F2; do
  scp jjmcd@192.68.0.17:${DIR}templates/${F} templates/
done
scp -r jjmcd@192.68.0.17:${DIR}templates/help templates/
scp -r jjmcd@192.68.0.17:${DIR}templates/spoolhelp templates/
scp -r jjmcd@192.68.0.17:${DIR}templates/skel templates/
