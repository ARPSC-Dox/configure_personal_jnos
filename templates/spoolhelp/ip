
ip <subcommand>

     These commands are used for the Internet Protocol service.


    ip access <permit|deny|delete> <proto> <sourceaddr[/bits]|all>
         <destaddr[/bits]|all> <iface>  [loport | all [hiport]]

     Display or set ip access controls. The ip access command controls packet
     routing via the specified <iface> by determining which source ip
     addresses <sourceaddr> are routed to which destination ip addresses
     <destaddr>.  If no ip access commands are issued for <iface>, the default
     behaviour is to permit all sources to access all destinations.  But once
     an IP access command is entered for <iface>, all routes via <iface> that
     are not specifcally permitted by an ip access command, will be denied.

     Execution of this subcommand will add or delete an access control
     entry in an internal table.  Incoming packets that would be routed via
     <iface> are compared with the table entries for <iface>, in the order that
     they were added, to determine if access will be granted (and routing take
     place).  Access will be granted only if an entry matching <destaddr>
     and <sourceaddr> is found with "permit" set before either a match with
     "deny" set is found, or the end of the table is reached.  The optional
     /bits suffix to the ipaddr specifies how many leading bits in the ipaddr
     are to be considered significant in the routing comparisons.  If not
     specified, 32 bits (i.e., full significance) is assumed.  All addresses
     can be specified by "all".  Access can be made protocol dependent via the
     <proto> parameter. <proto> may be 'a' for any, 't' for TCP, 'u'
     for UDP, 'i' for ICMP, or the IP protocol number. For UDP and TCP
     protocols, loport and hiport specify the port or range of TCP or UDP
     ports for which the access control command applies.  If none or all is
     specified, all ports are assumed.

     "ip access" will display the table of current access control entries.

     Access commands should be entered from the most specific to the
     least specific, since the first match (permit or deny) encountered
     for a given interface in the internal table is definitive.

     #Example:
         #allow a specific AMPRnet host access to the internet	
         ip access permit any 44.76.1.199 all eth0
         #but deny all others except UDP (eg, DNS) access
         ip access permit udp 44/8 all eth0 all 
         #permit only AMPRnet hosts access to RF port
         ip access permit any 44/8 44/8 2m
          

    ip address [<addr>]

     Display or set the default local IP  address.  This  command
     must  be  given before  an  'attach' command if it is to be used
     as the default IP address for the interface.


    ip encap [4 | 94]

     Display or set the packet ID code used for transmitted IP-IP
     encapsulated packets.  As of 1 March 1995, the default pid is 4.


    ip heard

     Display the ip-heard list. This shows the recently heard tcp/ip
     systems. See also the 'ip hport' and 'ip flush' commands.


    ip flush

     Clear the ip-heard list.  See 'ip heard' and 'ip hport'.


    ip hport [<iface> [ON | off]]

     Display or set the ip-heard facility.  If no argument is given,
     show the interfaces on which ip-heard is currently active. If
     <iface> is given, shows the status of the ip-heard flag for the
     given interface. If <iface> <on|off> is given, it will set the
     flag on or off.   Default is on.
     
     If this flag is on, ip heard frames will be logged in a table.
     This table can be shown with the 'ip heard' command or with the
     mailbox 'IHeard' command.  Ip-heard logging on ax.25 interfaces
     logs all ip stations heard on the port, even if the system wasn't
     directly involved in the ip activity.  For non-ax.25 interfaces,
     only ip frames that we were actively involved in (i.e. that we
     routed) are logged. (this difference is due to code internals)
     
          ip hport port1 off


    ip hsize [n]                                       Default: 16

     Display or  set the maximum size of  the Ip heard table. 0 means
     no limit.
     

    ip rtimer [<seconds>]                              Default: 30

     Display or set the IP reassembly time-out.
     

    ip status

     Display Internet Protocol (IP) statistics, such as total packet
     counts and error counters of various types.

    ip ttl [<hops>]

     Display or set the default time-to-live value placed in each
     outgoing IP datagram.  This limits the number of switch hops the
     datagram will be allowed to take.  The idea is to bound the
     lifetime of the packet should it become caught in a routing loop.
     You should make the value slightly larger than the number of hops
     across the network you expect to transit packets.  The default is
     set at compilation time to 255, the official recommended value
     for the Internet.


