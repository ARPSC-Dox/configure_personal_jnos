
convers <subcommands>

     These commands configure the network conference server. See the
section 'SETTING UP THE CONFERENCE BRIDGE' in the JNOS documentation for
details on the inner workings of the conference system.  NOTE: in the
JNOS40 CONFIGURATION MANUAL, the relevant section is called 'Configuring
the Conference Bridge'.


    convers channel [<default_chan_number>]           Default: 0

     Displays or sets the default channel number, that is, the initial
     channel to which new users are assigned.

          convers channel 2
          

    convers drop [<addr>]

     Drop the remote convers link to <addr>.  See also 'convers link'.
     If <addr> is not given, all links are dropped.

          convers drop 44.26.1.19
          

    convers filter                           Default: refuse (nobody)
     Set how the convers node will respond to connect requests.
     
       convers filter mode [accept | refuse]

        Sets or displays the filter mode.  'filter mode accept' allows
        links from only the hosts in the filter list.  'filter mode
        refuse' allows links from all hosts except those in the list.
        
       convers filter [ipaddress | hostname]

        Builds the filter list used in conjunction with the 'convers
        filter mode' command.
        

    convers host <name>

     Displays or set the convers hostname as will be used when
     announcing the system to conference users or remote links.
     Maximum length is 10 chars, but if you want to stay compatible
     with NOS.EXE based convers servers use a maximum of 8 character
     for the convers host name (unless the system runs jnos-v1.04 or
     later).
     
     If the 'hostname' gets set and the 'convers host'  isn't set yet,
     it will be set to the first 10 chars of the 'hostname'. After
     this, if any sub domains (i.e. periods) exist in the hostname,
     the convers hostname will be terminated at the right-most period.
     e.g. If  'converse host' is not set, and 'hostname
     jnos.wg7j.ampr.org' is given, then after this the converse
     hostname will be 'jnos.wg7j'.
     
          convers host Corvallis
          
          
    convers interface [<iface>] [on|OFF]

     Displays or sets the active convers interfaces. This command
     needs to be given for each interface that which will allow
     connections to the conference call (see 'convers mycall'); e.g.,
     this command can be used to allow conference call access only on
     the user ports but not on the backbone/linking ports.  This can
     also be useful to avoid confusion when different nodes have the
     same conference call.  (Locally, we use the call 'QSO' for the
     conference server for different nodes, and ran into problems when
     a user tried to connect to it from a backbone node. All of a
     sudden two nodes were answering the connect !)  Default is off.
     
          convers interface port1 on
          
          
    convers link [<addr> [port] [name]]
    convers xlink [<addr> [port] [name]]

     If no <addr> is given, display the list of linked nodes with
     link status and statistics indicated.  If <addr> is given,
     add a convers link to another (remote) conference server.  The
     link is LZW-compressed if xlink, instead of link, is specified
     (and XCONVERS was defined at compile-time).

     <addr> is the ip address or hostname of the remote server to which
       to link.
     [port] is the tcp port number to use for the server connection.
     [port] defaults to 3600 if the link command is used, and 3601 if
       the xlink command is used.
     [name] is the optional name that will show up in the links
       listing shown with the '/links' command if the link has not yet
       been established. [name] can be a maximum of 10 characters; the
       first character must be a non-integer.
     
     After the link has been established, the name will be set to the
     name with which the remote system introduced itself.
          convers link 44.26.1.19 Testing
          

    convers [u|h]maxq [<bytes>]

     Display or set the upper limit for the number of bytes that can
     be queued up waiting for transmission on a connection to another
     server.  If there is more data than this limit, the connection to
     the other server will be closed.
     
     You are able to set individual limits for users and hosts
     with 'convers hmaxq' and 'convers umaxq'.  If set to 0, there is
     no limit, otherwise connections will be reset if there is more
     than the []maxq value data outstanding on the connection.  The
     connections will be RESET instead of gracefully closed.

     Default values are umaxq of 1024 and hmaxq of 5120.  Note that any
     changes will only affect new connections, not existing connections.
     

    convers maxwait [<seconds>]                        Default: 10800

     Display or set the upper limit for the time the system will wait
     to reestablish a disconnected convers link that originated at
     this system. Time is given in seconds.
          convers maxwait 600
          
          
    convers motd ["<yourmessage>"]

     Set or show the message of the day for the convers server.  This
     message is displayed when users connect to the server.
     NOTE: this option is obsolete in recent convers implementations.
     Instead, the contents of the file /spool/convmotd.txt are displayed.
     This filename can be changed by setting the ConvMotd string in nos.cfg.
     
    convers mycall <mycall>

     Display or set the 'conference call'.  <mycall> is a separate
     ax.25 callsign.  If set, users can connect to it to get
     immediately connected to the conference bridge. However, each
     port or interface that this call should be allowed on should be
     enabled with the 'convers interface' command.  Conference call
     connections bypass the regular node interface.  This is
     independent from the settings of 'mbox convers' or whether the
     network conference server has been started. See also 'convers
     t4'.
          convers mycall QSO
          
          
    convers online [long | call | @host]

     Display a list of convers users known to the convers server.  This
     is the same report as a /who listing made from within the convers
     facility.  The default report is a "quick" format listing of the
     connected users.  The "long" option specified a long-format report,
     which can be restricted to a particular "call" or "host".
     Example:  conv on @luzana   -or-   conv on wu3v   -or-   conv on


    convers setinfo [yes | NO]

     Display or the set the ability of conference users to change their
     personal info as stored in /finger/dbase.dat.  This sub-command is
     only available when CNV_CHG_PERSONAL was defined when Jnos was
     compiled.

          
    convers t4 [<seconds>]                             Default: 7200

     Display or the set the conference call connection T4 timer.
     t4 is the 'redundancy timer' for ax.25 connections to the
     conference server.  This allows you to set a different inactivity
     time-out for ax25 node and conference connections. Default is
     7200, i.e. 2 hours.
          convers t4 900
          

    convers tdisc [<seconds>]                           Default: 0

     Display or the set the conference call general redundancy timer
     that applies to all connections to the conference server.
     Connections which are idle longer than <seconds> will be
     disconnected.  Default is 0, i.e. disable idle timeouts.
          convers tdisc 1200



    Files used by the convers facility:
     Keyword       Default value        Usage
     ConvMotd      /spool/convmotd.txt  Connect greeting.
     Cinfo         /finger/dbase.dat    Convers user personal info
     Cinfobak      /finger/dbase.bak    Previous Cinfo file (after update)
     Channelfile   /spool/channel.dat   Channel number to name mapping
     --            /spool/help/convers.hlp Help file (/help command)

    Cinfo line format:  <name|call> <personal data, ie, QTH, etc>
    Channelfile line format: <channel_number> <channel_name>


    Note: Converse users often wonder why the /help report is not
    complete. This is because the default compilation options for
    convers.c include "noblocking".  This results in Jnos dropping
    data destined for any output queue that exceeds certain length
    limits, and thus ensures that Jnos will not run low on buffer
    space due to the inability of a slow RF link to process the
    output queue fast enough.  The limits at which lossage occurs
    depends on how a user connects to Jnos: telnet users are limited
    by the tcp window size, ax.25 users are limited by the Jnos 'ax25
    window' setting, and the local console is limited by the LOCSFLOW
    constant (2048).  The convers.c source could be editted to #undef
    noblocking, and recompiled in an attempt to rectify this limitation,
    but (worse?) instability might result!

