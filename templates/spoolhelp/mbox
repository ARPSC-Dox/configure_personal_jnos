
mbox [<subcommands>]

     Without a subcommand, display the current users of the mailbox.
With a valid subcommand, it will execute the following commands.

     
    mbox alias [<alias>] ["cmd"]

     Set or show alias commands for the mailbox.  To use an alias, the
     mbox user must type the full alias which will then cause "cmd" to be
     executed.  If the mbox user types 'ALI', a list of sysop-defined aliases
     will be displayed.  'alias' is 6 characters maximum.  "cmd" is 64
     characters maximum.  Note that "cmd" is in double quotes, and is
     interpreted as a mailbox command unless it begins with '@', in
     which case it is a console command.  Only console commands that
     don't spawn sessions, and that don't require special privileges,
     are allowed.
     
     'mbox alias' displays the current aliases.
     'mbox alias <alias>' displays only <alias> if it is defined.
     'mbox alias <alias> <cmd>' adds, deletes, or redefines <alias>
        
        Example:  To add a new BBS command -
          mbox alias bbs "c port bbscall"
          
        To delete an alias-
          mbox alias myalias ""
          
        To redefine an alias-
          mbox alias bbs "c newport newbbscall"

        To invoke the console pkstat command-
          mbox alias pkst "@pkstat"

     See also "mbox showalias", below.

     
    mbox attend [ON | off]

     This displays or sets the attend flag. If set, users can initiate
     chat with the sysop via the O)perator mailbox command.
     (You need to have started the ttylink server for this !)
     
     
    mbox bbsonly [iface] [on | OFF]

     Specify whether iface is only accessible to stations which have
     the BBS flag set.
     
     
    mbox convers [ON | off]

     Display or set the access to the conference bridge.  The mailbox
     'CONV' command is disallowed when off.  Default is ON.

     
    mbox fbb [0 | 1 | 2]

     Display or set the flag which enables FBB batched-style forwarding (1)
     or FBB compressed and batched forwarding (2), or standard WA7MBL-style
     forwarding (0).  The setting is used to determine which forwarding
     capabilities we proposed to a another station.  The default value is
     determined by whether Jnos was compiled with FBBCMP and/or FBBFWD
     #define'd.  In any case, the forwarding method actually used is the
     intersection of capabilities of both stations involved in forwarding.
     Batched forwarding allows the forwarding direction to be reversed every
     5 messages.  Compressed FBB forwarding requires a completely transparent
     connection and uses Lempel Ziv Huffman compression.


    mbox fwdinfo [string]

     Displays or sets the string that is used in the BBS R: header
     when forwarding mail to other BBSes.
     
     
    mbox haddress [string]

     Displays or sets your system's hierarchical bbs address.  As of
     version 1.08, you must include the bbs call with the hierachical
     address.

                    Example:
          mbox haddress wg7j.or.usa.na
     
     
    mbox header [on | OFF]

     Explicitly turns the R: line in the message header on or off.
     With mbox header off, regular users can forward from jnos to
     another system designated as a full-service bbs without leaving a
     trace to their system.  This avoids having a downstream bbs
     improperly identify the originating station as a bbs.
     
     The elements of the R: header are optional as of jnos107.  The
     line has this format (where undefined elements will be dropped):
     R:yymmdd/hhmmz @:haddress [qth] fwdinfo #:mid $:bid Z:zip
     
     NOTE:  'mbox header off' also turns off the mbox third-party flag.


    mbox hideport [<iface>] [on | OFF]

     Display or set a port that should not be displayed when the 'P'
     command in the mailbox is given.  Display is always available to
     users with sysop permissions.  This command is useful for ports
     that should be backbone linking only, etc.  Note that currently
     users can still do ax.25 connections via that port, if they know
     the name. Default is OFF, the port is displayed.

          mbox hideport port1 on
     

    mbox holdlocal [on | OFF]

     Display or set the flag indicating if locally-originated messages, or
     messages arriving via an SMTP-protocol connection, are to be held for
     sysop review before being forwarded or readable.
     Only messages written to areas listed in /spool/holdlist are affected.
     The sysop would typically use the LH and RH commands to review held
     messages and approve the removal of the hold status.  This command is
     enabled only when HOLD_LOCAL_MSGS is #defined at compile-time.

     If invoked to display the hold flag status, and the status is ON,
     then all areas listed in the Holdlist file that contain held messages,
     are displayed, along with the total number of held messages.


    mbox ifilter [on | OFF]

     Display or set the spurious I-frame filter flag, which when set will
     prevent unexpected I-frames sent to mycall from spawning a mailbox
     session.  The effect of setting this flag is to deny access to the
     mailbox unless our ax25 bbscall, ax25 ttycall, convers mycall, ax25
     alias or netrom alias is used.  Mbox ifilter is necessary to send IP,
     Net/ROM, and segmentation frames over ROSE circuits.


    mbox kick [<bbscall>]

     This is an immediate command. It forces the system to start a new
     BBS forwarding cycle.  All BBSes in forward.bbs are examined unless
     <bbscall> is given, in which case only that entry in forward.bbs is
     considered.  If <bbscall> is given in uppercase, a poll will be done;
     otherwise, a connect is attempted only when there is traffic to send.
     Example:  at 35 "mbox kick w5ddl+"          kicks just w5ddl at hh:35
               mbox kick K5ARH                   forces an immediate poll

     
    mbox mailfor [interval]
    mbox mailfor exclude [areaname areaname ...]
    mbox mailfor watch [call_1 call_2 ...]

     Interval is in seconds.  At the end of each interval,  the system
     reads all non-area mailboxes and checks them for unread mail.
     Next, a beacon is sent on all active interfaces. (see 'mbox
     mport' command).  The beacon is addressed to the AX.25 address
     'MAIL'.  The data in this packet contains a list of the mailboxes
     with unread mail in the format 'Mail for: <user> ... '.  Systems
     such as LAN-LINK can trigger mail-snatches from this beacon.  If
     <interval> is the string "now" an immediate check is done.

     'mbox mailfor' will show the status of the timer and list
     mailboxes with unread mail.

     In certain cases you might not want a private area to show up in
     the mail beacon; e.g., private areas to be forwarded to another
     bbs.  You can exclude such areas from the beacon with the
     'mbox mailfor exclude' command.


     'mbox mailfor exclude area1 area2 area3 ...'
     
     will disable these area names from the beacon.  You can give
     multiple exclude commands to build the list.

     A simple 'mbox mailfor exclude' shows the list.

     'mbox mailfor watch call ...' will watch for new mail arriving
     for the specified personal mailboxes, and add a blinking MAIL to
     the first status line if new mail exists.  To remove the MAIL from
     the status line, read the last message in the appropriate mailboxes,
     and force a run of the mailfor process by 'mbox mailfor now'.  The
     sysop will typically want to watch his personal mailbox, and perhaps
     a "check" mailbox to which unusual mail is rewritten.


    mbox mailstats

     This command shows how many messages have been read and sent by
     users and how many messages have been received from and forwarded
     to BBSes.  Also shown is the current amount of "core" memory un-
     used, and the number of logins, current user count, and number of
     different users seen since Jnos was booted.
     
     
    mbox maxusers [N]                              Default: 0
     Display or set the number of simultaneous mailbox users permitted.
     A value of zero disables this feature.  A mailbox connection which
     would exceed the maximum permitted user count, is disconnected
     with the error message "Too many mailbox sessions".  Note that both
     incoming and outgoing connections, even forwarding sessions, are counted.


    mbox mport [<iface>] [on | off]

     Displays or sets the interfaces for 'MAIL' beacons.  Set this
     flag for each port you want the mail beacon to be sent on.


    mbox newmail [ON | off]

     When ON, users will be notified during login which areas have new
     mail since the last time that user logged out.  This function
     uses a time stamp on a status file for each message area.  The
     information shown can later be repeated with the 'AN' mailbox
     command.
     

    mbox noax25 [iface] [on | OFF]

     Specify whether iface is only accessible to stations connecting
     by other than the ax.25 protocol.  Connections to the converse
     call are not affected by this command.
     
     
    mbox nobid [on | OFF]

     Set whether to accept or refuse bulletins with NO BID.  Off means
     refuse if there is no BID.  On means accept regardless of BID.
     Default is OFF (refuse)
     
     
    mbox nrid [ON | off]

     When set, the first time a user logs onto the system, the system
     will add the netrom node id in NODE:CALL format to the prompt.
     Users can then change it with the mailbox XN command. The system
     will use the new prompt format as set by the user the next time
     the user logs in.
     
    

    mbox past [<N> | flush]

     Displays the users that have logged on since the system has been
     running.  If an integer <N> is provided, only the most-recent N
     users are displayed.  If "flush" is provided, the list of past
     users is cleared.
     
     
    mbox password <newpassword>

     This sets a new remote sysop password.  A remote sysop is a user
     whose entry in the ftpusers file has the SYSOP_CMD bit set.  When
     a remote sysop enters the '@' command to the Jnos mailbox, and there
     is a non-null mbox password established, five random numbers are
     displayed.  The remote sysop is expected to then transmit the letters
     corresponding to these numbers, taken as zero-relative positions in
     the password string.  Several lines of five letters can be sent, only
     one of which need be correct.  The last line sent must be empty, ie,
     just a CR.  If the response is correct, the remote sysop is then given
     the Jnos command-line prompt, and may issue most Jnos console commands.
     Commands which would require creation of a new session are disallowed.
     Use the "exit" command to exit from the Jnos command level.
     
     
    mbox qth [location]

     This displays or sets the location of your system, and uses it in
     the R: headers when doing bbs forwarding.
     
     
    mbox register [ON | off]

     Enable or disable user registration.  If the user gives an e-mail
     address during registration, messages sent will include a 'Reply-
     To:' header with that e-mail address.  Setting this command to
     OFF will prevent the 'please register' message from being sent to
     new users.
     
     
    mbox reset [login_id_list]

     Force a disconnect for the named user(s), or list the names of the
     users currently connected if no argument is provided.


    mbox secure [on | OFF]

     This displays or sets the mailbox secure flag. If set, only users
     coming on over netrom and ax25 connections can make gateway
     connects if they have the right privileges. If not set, anyone
     with the right privs can do a telnet connect, provided that their
     login name passes the valid-callsign tests within Jnos.
     
     
    mbox sendquery [ON | off]

     If on, users will be queried if they really want to send the
     message after they have typed the ^Z or /ex when sending a
     message. 'N' or 'n' will abort, anything else will send the
     message.
     
     
    mbox showalias [on | OFF]

     Set or display the flag which determines whether aliases (if
     defined) are listed in the mbox prompt.


    mbox smtptoo [on | OFF]

     This displays or sets the smtp header flag.  If set, the system
     will include most smtp headers when forwarding the message via
     bbs forwarding.  When not set, the headers will be stripped
     (default).
     
     
    mbox status

     Displays the current users of the system {NOS}.
     
     
    mbox sysoponly [iface] [on | OFF]

     Specify whether port is accessible only by stations with SYSOP
     flag set.  Default is off.
     
     
    mbox timer [<nnnn>]

     Entering only the subcommand displays the forwarding setting and
     countdown timer value.  [<nnnn>] sets the interval between
     forwarding attempts in seconds.  Default = 0 (No forwarding).

     When the timer matures, the forward.bbs file is scanned for entries
     that match the time constraints, and either have msgs to forward or
     have the poll flag set on their entry. A process is started to initiate
     forwarding with each matching entry, all processes begin at once!
     Finer control of forwarding times can be obtained by keeping the
     timer at 0, and using the at command to issue repeated 'mbox kick'
     commands for the desired forwarding partners.  For example,
     at 05 "mbox kick k5arh+"    will try to forward to just k5arh at
     five minutes after each hour.
     
     
    mbox tdisc [<nnnn>]

     This command sets the mailbox inactivity timer in seconds.  If no
     user input has been received for nnnn seconds while connected to
     the mailbox, the user will be disconnected.  Default = 0 = no
     timeout.
     
     
    mbox tmsg [<string>]

     Display or set the 'telnet message'.  This is the message sent to
     telnet connections before the login prompt is sent!
     
     
    mbox trace [on | OFF]

     This displays or sets the value of the trace flag. If set, the
     mailbox is verbose about certain aspects of the forwarding cycle.
     
     
    mbox usersonly [iface] [on | OFF]

     Specify whether iface is only accessible to stations which don't
     have the BBS flag set.
     
     
    mbox zipcode [<nnnnn>]

     Set or display the system's postal zipcode.  This is used in the
     R: line when forwarding.
     
