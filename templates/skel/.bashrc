# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions
THISTERMINAL=`tty`
if [ "$THISTERMINAL" == "/dev/tty6" ] ; then
  cd /jnos
  sudo ./startnos
  logout
fi
