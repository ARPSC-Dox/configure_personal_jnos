
status [on|off]

        The 'status' command displays general system information, attended
        flag, and a table of open files (when possible).  This is useful
        for monitoring file transfer activity.

        If STATUSWIN and STATUSWINCMD were #define'd when Jnos was compiled,
        the status command can take an optional argument, which determines
        whether the status window is turned on or off.  The status window
        must be enabled when Jnos is started (see the -u, -w and -x command-
        line argument descriptions).  The 'status on|off' command can then
        be used to disable (via "off"), or re-enable (via "on"), the status
        window at the top of the screen.  When the status line state is
        changed, existing sessions will have their screen cursor repositioned.

        The status window can have up to 3 lines.  The first line shows
        the time, heap/dos available memory, number of connections to various
        services, and then a list of active sessions, where sessions with
        data waiting to be read are blinking.  A blinking MAIL shows when the
        'mbox mailfor watch' command detects unread email.  When Jnos contains
        a mailbox server, the next (second) line shows the users connected to
        the bbs. A status symbol precedes the user's id:
            none - user is idle
            * - user is a bbs
            @ - user is in sysop mode
            ! - user has gatewayed out
            # - user is reading or sending mail
            = - user is transfering data (up/download)
            ^ - user is in convers or sysop-chat mode
            ? - use is in none of the above, but not idle...

        The final line, when present, shows data depending on the current
        session.  The current session number and type are always displayed.
        If the session is a network connection, the remote connection name,
        tx-queue (bytes for tcp, packets for ax.25), and state of the
        connection are displayed, followed by the retry timer, with current
        time left, and initial value.  In repeat, more or look sessions, the
        third line shows the command, filename or user/socket for the session.
