sessmgr [<subcommands>]

    The sessmgr command sets or displays aspects of the Jnos unix session
manager and existing sessions.  Depending on what compile-time options were
selected, Jnos supports curses, raw and dumb managers.
    Curses supports options "tty", "notty", "bold", "nobold", "8bit" and
"no8bit". "ansi" is a synonym for "notty" and "noansi" is a synonym for "tty".
The "tty" option turns off VT102/ANSI emulation, resulting in a "dumb crt" mode
of operation.  The "bold" option changes text highlighting from reverse-video
to high-intensity display.  The "8bit" option passes character codes > 127 to
the display; the default "no8bit" mode only allows some of the line-drawing
charcter codes > 127 to be accepted.
    The Dumb session manager doesn't interpret escape sequences, and since
it has no function keys, it interprets ^C as F10 and ^T as F9, that is,
^C switches to the command session, and ^T switches to the trace session.
Use the session command in the command session to switch to other sessions.
    The Raw session manager does not provide VT102 emulation, and so relies
on the Linux tty driver to interpret escape sequences.

    sessmgr create <sessmgr> <jnos_command> [<arguments>]

     Creates a new session for <jnos_command>, managed by <sessmgr>.
     This is equivalent to typing <jnos_command> and any arguments, having
     set the default session manager to <sessmgr>.  Presently not all
     session-creating Jnos commands are available.

    sessmgr default [<sessmgr>]

     Displays or sets the value of the default session manager.  As
     mentioned above, <sessmgr> is one of "curses", "raw", or "dumb".
     The default session manager applies to sessions created by typing their
     associated command names in the command window.  Note that options can
     be associated with <sessmgr> by appending a colon (:) followed by a
     comma-separated list of options.  Example:  curses:8bit,tty

    sessmgr options <s#> [<options>]
     Displays or sets the options for session number <s#>.

    sessmgr reparent <session> <sessmgr>
     Change the session manager to <sessmgr> for session <session>, which
     must be uniquely specified by number, name, or type, or may instead
     be the name of a session manager, which results on all sessions having
     that session manager being reparented to the new session manager
     <sessmgr>.

    sessmgr status
     Display info about the available session managers, and which sessions
     exist, with their managers and options indicated.
