
bulletin <subcommand>

     The 'bulletin' command establishes how Jnos treats incoming
bulletins based on their R: line headers.  These commands are available
when Jnos was compiled with RLINE defined, and are highly recommended for
BBS operations.  While many defaults are OFF for historical reasons, I
think most BBS operators will want to turn these features ON!


    bulletin check [on | OFF]

     Displays or sets a flag indicating bulletin forwarding is to be
     optimized.  If <ON>, an incoming bulletin's R: lines are scanned for
     BBSes to which we forward.  If any are found, the bulletin is marked
     as having already been forwarded to that BBS.  When invoked with no
     options, the list of BBSes to which we forward is also displayed.
     Note: only the first NUMFWDBBS (currently 8) BBSes in forward.bbs
     are checked in the R: lines, hence the most active BBSes should occur
     first in forward.bbs.


    bulletin date [on | OFF]

     Displays or sets whether the date associated with a message is
     the originate date (if ON), or the date received (if OFF).  The
     originate date is obtained from the last R: line, and should
     yield better operation of the bulletin expire mechanism.  See also
     'bulletin holdold'.


    bulletin holdold [#days]

     Displays or sets the number of days since message origination, above
     which an incoming bulletin is placed into a hold state. A held bulletin
     is visible only to the sysop, and has a "X-BBS-Hold: Age" header added.
     This feature only works when 'bulletin date ON' is in effect.


    bulletin loophold [<count>]                              Default: 2

     Displays or sets a counter which, if exceeded by the number of times
     our call appears in a message's R: line headers, causes the message
     to be Held to avoid a message-forwarding loop.  A held message is
     visible only to the sysop, and has a "X-BBS-Hold: Loop" header added.


    bulletin return [on | OFF]

      Displays or sets how a message's return address is obtained.
      If ON, the return address is obtained from the last R: header line
      when available.  If OFF, sender%forwarding.bbs@mycall is used.

      Note that for the mailbox SR (send-reply) command to work, the
      system's rewrite file must be capable of handling the '%' symbol.
      A recommended rewrite rule is: *%*@YOURCALL* $1@$2 r


