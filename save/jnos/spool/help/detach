
detach <interface>

        Detach a previously attached interface from the system.  All IP
        routing table entries referring to this interface are deleted,
        and forwarding references by any other interface to this
        interface are removed.

        Because of certain design limitations, an interface should be
        detached only while it is idle, otherwise there is a (small) risk
        that an invalid interface pointer could be used to modify memory
        resulting in a crash (under UNIX) or low-memory-address corruption
        under MSDOS.  For example, a packet may have to be retransmitted
        after an ack-timeout, but if the interface no longer exists Jnos
        gets confused...

	Neither the encap nor the loopback interfaces can be successfully
	detached.  However, beginning in Jnos 1.11e, a detach of the encap
	interface does result in all associated routes being deleted.
	This should aid those gateway systems needing to update their
	encapped routes periodically: detach encap, then issue the new
	route commands.
