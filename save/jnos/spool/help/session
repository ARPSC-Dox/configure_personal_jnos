
session [<session#>] [arguments]

     Without arguments, displays the list of current sessions,
including session number, associated socket, the session screen-swap
mode, the session type and state, the send and receive queue sizes,
and the remote TCP or AX.25 address.  An asterisk (*) is shown next to
the current session.

     Entering  a session number as an argument to the session command
will put you in converse mode with that session.  If you have started
the TTYLINK server, and have set Attended mode, an incoming telnet con-
nect to the ttylink port will automatically create a split-screen
session and switch you to that session.


    session <session#> flowmode [on | off]

     <flowmode> displays or enables / disables setting of --more--
     handling for <session#>.  This is handy for long directory
     listings coming from an ftp session, for example .  Escaping to
     command mode before issuing the dir command and entering "session
     # flowmode on" gives a page at a time to look at.  At any time
     you can escape out again and switch flowmode off.  Note that a
     ftp session has it's own flow command now built in.  See FTP
     commands later in this manual.

     To avoid the --more-- prompt when the output of console commands
     exceeds the screen length, turn flowmode off for session 0.  This
     session number always exists, but is not shown in the session
     listing.  Some commands, such as "more" or "dir", will ignore
     flowmode since they manage their own output pagination.  But, these
     commands will accept a 'Q' to quit (abort) further output.
     Example:  session 0 flowmode off


    session <session#> split [on | off]

     <split> displays or enables / disables the split-window capability
     of session <session#>.  This is useful to separate what you enter
     from the keyboard (shown in a small window at the bottom of the
     screen) from incoming data, which is shown in a large window at the
     top of the screen.  For example, a ttylink session is begun in split
     mode.


    session swapmode [ems | xms | memory | file]

     <swapmode> displays or defines how session screens are saved when
     the associated session is not current.  The initial setting is
     determined by the -m command-line argument value and/or compilation
     options.
