
ftp <hostname> [<scriptfile>]

    The ftp command is used to make a TCP connection with <hostname>, and
    then use File Transfer Protocol to exchange data between the systems.
    Once the connection is established, a small set of commands is used
    to manage the file exchange.  A command is executed locally if it is
    a local client command.  Otherwise, the command is sent to <hostname>
    for execution.  If <scriptfile> is provided, all commands are obtained
    from the indicated file; otherwise, they are read from the console.
    The following are commands supported by Jnos clients and servers:

     ?		Display all available command names.
     help       Same as ?

     ascii      Treat the data to be transferred as ASCII text, so that
                line endings are used suitable to the receiving system.
     binary     Treat the data to be transferred as binary data, that is,
                verbatim data not to be changed while storing on the
                receiving system.
     type [a|b|l] Query the current transfer type value, or set it if <a|b|l>
                is given.  Use <ascii>, <binary> (or <image>). <logical 8>
                is also supported.

     reclzw [y|n] Query the state of the ftp client LZW-supported flag,
                or set it if <y|n> is supplied.  LZW compression is only
                supported for ASCII-type transfers.
     sendlzw [y|n] Query the state of the ftp server LZW-supported flag,
                or set it if <y|n> is supplied.  LZW compression is only
                supported for ASCII-type transfers.

     batch [y|n] Query the state of the command batching flag, or set it
                if <y|n> is given.  Batching involves sending as many
                commands as possible before waiting for responses from
                <hostname>.
     hash [y|n] Query the state of the hashmark flag, or set it if <y|n>
                is given.  Hashmarks are written to the screen for each
                1000 bytes written to the local file system.
     verbose [n] Query verbosity of error handler, or set it if integer <n>
                is given.  0 => error msgs only, 1 => final msg only,
                2 => control msgs too, 3 => control msgs + hash marks,
                4 => control msgs + byte counts.

     dir spec   List the contents of the current directory on the remote
                system, in a verbose manner.  If <spec> is given, the subset
                that matches this file specification is listed.
                    Example:  dir *.exe
     list	Same as dir command.
     ldir spec  Same as the dir command, but applied to the local system.
     ls spec    List just the names in the current directory on the remote
                system.  If <spec> is given, the subset that matches this
                file specification is listed.
                    Example:  ls *.exe
     nlst       Same as ls command.

     cd path    Change to directory <path> on system <hostname>.
     cdup       Change to immediately-superior directory on system <hostname>.
     lcd path   Change to directory <path> on the local system.
     mkdir dir  Create directory <dir> on the remote system.
     lmkdir dir Create directory <dir> on the local system. 
     rmdir dir  Delete (remove) directory <dir> on the remote system.

     get file   Transfer <file> from remote system TO the local system.
     mget spec  Transfer all files matching <spec> from the remote system
                TO the local system.
     view file  Transfer <file> from the remote system TO the local system's
                console screen.  The file is assumed to be an ASCII file!
     resume file Restart an interrupted transfer of <file> from the remote
                system to the local system.  Checks are made to assure the
                file is consistent between systems.  This is a JNOS extension
                to the FTP standard.  Other non-compatible equivalents exist
                in other implementations (c.f. wu-ftpd).
     reget file The RFC959-endorsed way to restart an interrupted get.  No
                check is made to assure the file is consistent between systems;
                the size of the local <file> is provided to the remote system
                and a get is issued relative to this point.

     put file   Transfer <file> to the remote system FROM the local system.
     mput spec  Transfer all files matching <spec> to the remote system
                FROM the local system.
     rput file  Resume an interrupted transfer of <file> to the remote system
                FROM the local system.  Checks are made to assure the
                file is consistent between systems.  This is a JNOS extension
                to the FTP standard.  Other non-compatible equivalents exist
                in other implementations (c.f. wu-ftpd).
     restart pos The RFC959-endorsed way to restart a transfer is to first
                establish a starting offset position and then issue a transfer
                command to resume at that position.  The dir command would
                be useful to obtain the offset to specify in the restart
                command, and then a put <file> would cause <file> to be sent
                starting from the given position.

     dele file  Delete <file> on the remote system.
     rename from to  Rename the file named <from> to the name <to> on the
                remote system.

     mdtm file  Display the modified-time in GMT for <file> as yyyymmddhhmmss.

     quit       Close the TCP connection to <hostname> and exit the ftp cmd.
                See also the Jnos "abort" command.


    Since unrecognized commands are sent to the remote ftp server for
    evaluation, additional commands may be available, depending upon the
    ftp server implementation.  For example, the WU-FTPD may accept site-
    written extensions, and thus allow:  site exec <extended_cmd> <cmd_args>.

    The remote ftp server will require a login name and password.  These
    values may be provided by a file called "net.rc" by default (see
    Hostfile in nos.cfg).  The file has entries in this format:
    remote_hostname   login_name   password

    but password may be omitted to instead have the client ftp prompt for
    it.

    Many systems, including Jnos, will reduce the amount of extraneous
    messages sent, if the password is prepended with a '-'.  However,
    Jnos does not support this when an anonymous login occurs via an MD5
    authentication exchange.

