
ax25 <subcommands>

     All AX.25 parameters are configurable per interface.  Commands of
     the form 'ax25 <command>' set the default or global values.  Use
     the 'ifconfig <iface> ax25 <command>' form to set or show the
     interface-specific values.

     To set the system default ax.25 parameters, you must do so BEFORE
     attaching interfaces.  After attachment, you must use the 'ifconfig
     <iface> ax25' command form to show or change values for that interface.
     THIS IS A CHANGE FROM THE BEHAVIOUR EXHIBITED PRIOR TO JNOS VERSION
     1.10X16, RELEASED 08FEB94.


    ax25 alias <aliascall>

     The alias command shows or sets the system's alias call. If
     netrom is enabled, this modifies the same call as the 'netrom
     alias' command.  The 'ax25 alias' command is NOT needed in that
     case!  If netrom is not used, this command allows an alias name
     to be set such that users can connect to it.  The alias is typically
     a short string and should not be a valid callsign, since any SSID
     is ignored when matching against <aliascall>.
         Example:  ax25 alias knx
     

    ax25 bbscall [<bbs_call>]

     The bbscall subcommand sets or shows the current bbs callsign.
     Connects to this callsign, when set properly, will "jump-start"
     the mailbox.  That is, after the connect no additional packet
     need be sent to obtain the mailbox greeting.  This subcommand
     will automatically set the bbscall for all currently-attached
     AX.25-class interfaces with no bbscall set.  For bbscall to
     function properly, it must differ from the system alias/netrom
     alias, as well as the link address of the interface (usually
     set by 'ax25 mycall').

     See also 'ifconfig <iface> bbscall <bbs_call>'.  Note that the
     bbscall value is also used for the source address of ax.25
     connections initiated from the console, provided that the ax25
     ttycall is not set, or that Jnos was *not* compiled with
     TTYCALL_CONNECT #define'd.

          #Example: (in the following order)

          'ax25 mycall N5KNX-1'
          'ax25 alias KNX'
          'ax25 bbscall N5KNX'
          'attach <(all interfaces)>'

          or
          
          'ifconfig <name> bbscall <bbscall>'   sets only iface <name>


    ax25 bc <iface>

     The bc command  forces an immediate broadcast on the given
     interface.  The particular interface or port must have been
     enabled with the ax25 bcport command first. If this is so, the ID
     will be broadcast as set with the ax25 bctext commands.
     
          ax25 bc port1
    

    ax25 bcinterval [<seconds>]

     The bcinterval displays or sets the time in seconds between
     broadcasts.  On display, both the interval and the countdown
     values are shown. Default = 600 (10 minutes).
     
     
    ax25 bcport [<iface> [on | OFF]]

     Display or set the active interfaces for ax.25 broadcasting (i.e.
     beacons).  If no interface is given, all interfaces with ax.25
     beaconing enabled will be listed.  If interface is given, the
     status of beaconing for that interface is shown, or set according
     to the following on or off option.  Initial state is off.
     
          ax25 bcport port1 on
          
          
    ax25 bctext ["broadcast text"]

     Display or set the default text to be sent for broadcast messages
     sent out every ax25 bcinterval seconds.  To compose a multi-line
     message, use \r between the text of each line, ie, "line1\rline2".
     See also 'ifconfig <iface> bctext ["bctext"].

          ax25 bctext "This is the beacon text!"
          
          
    ax25 blimit [<value>]                             Default: 30

     Display or set the default AX25 retransmission backoff
     limit.  Normally each successive AX25 retransmission is delayed
     by twice the value of the previous interval; this is called
     binary exponential backoff.  When 2^(retrycount) reaches or ex-
     ceeds the blimit <value>, the retry interval is no longer increased.
     
     To prevent the possibility of "congestive collapse" on a loaded
     channel, blimit should be set at least as high as the number of
     stations sharing the channel.  Note that this is applicable only
     on actual AX25 connections; UI frames will never be retransmitted
     by the AX25 layer.  See also ax25 maxwait, and ax25 timertype.
     
          #Set ax25 blimit to 15
          ax25 blimit 15          
          

    ax25 dest [<iface>]

     Display the destinations saved in the heard list, for all interfaces
     with heard list maintenance enabled, or for the just the specified
     interface.  See also "ax25 heard" and "ax25 filter N".


    ax25 digipeat [<iface> [ON | off]]

     Display or set digipeating per interface.   If no interface is
     given, all interfaces with digipeating enabled will be listed. If
     interface is given, the status of digipeating for that interface
     is shown, or set according to the following on or off option.  If
     cross-band or AXIP digipeating is to be allowed, digipeating must
     be enabled on both interfaces involved.  Default is on.
     
          # Display digipeat status of port1
          ax25 digipeat port1
          
          
    ax25 filter N					Default: 0

     Sets the ax.25 heard list filtering value.  This is a global value
     that affects all ports with heard list maintenance set to ON.
     0  =>  log both source and destination callsigns
     1  =>  do not log source callsign
     2  =>  do not log destination callsign
     3  =>  do not log any callsign, ie, disable all heard list updates
     

    ax25 flush

     Clears the AX.25 "heard" list (see ax25 heard and ax25 hport)
     

    ax25 heard [<iface>]

     Display the AX.25 "heard" list. For each interface that is
     configured to use AX.25 heard listing (see 'ax25 hport'), a list
     of all ax25_source addresses heard on that interface is shown,
     along with a count of the number of packets heard from each
     station and the time since each station was last heard. The
     maximum length of the heard table can be set with the 'ax25
     hsize' command.  If interface is given, only the heard list for
     that interface is displayed.

          ax25 heard port1
          
          
    ax25 hearddest [<iface>]

     Same as "ax25 dest [<iface>]".


    ax25 hport [<iface> [ON | off]]

     Display or set the status of the ax.25 heard feature.  If no
     interface is given, all interfaces with ax.25 heard enabled will
     be listed.  If interface is given, the status of ax.25 heard for
     that interface is shown, or set according to the following on or
     off option.  Default is on.
          
          #Display port1 status
          ax25 hport port1
          
          
    ax25 hsize [<size>]

     Set or display the size of the heard list table.  Default is 0
     which means no limit.
     
     
    ax25 irtt [<milliseconds>]

     Display or set the initial value of smoothed round trip time
     to be used when a new AX25 connection is created.  The actual
     round trip time will be learned by measurement once the
     connection has been established.  Default is 5000ms.
          #Set irtt to 10 seconds  (10000 milliseconds)
          ax25 irtt 10000
          
          
    ax25 kick <axcb>

     Force a retransmission on the specified AX.25 control block.  The
     control block address can be found with the ax25 status command.
     This is useful to reactivate connections that have long time-out
     values.
     

    ax25 maxframe [<count>]

     Establish the maximum number of frames that will be allowed
     to remain unacknowledged at one time on new AX.25 connections.
     This number cannot be greater than 7. Without <count> it will
     display the current setting. Note that the maximum outstanding
     frame count only works with virtual connections. UI frames are
     not affected. Also note that for optimal performance, a value of
     1 should be used.  Default is 1 frame.
     
     
    ax25 maxwait [<msec>]

     Sets a limit (in msec) to the retry timeout values.  Default =
     30000 (30 secs).  A value of 0 disables maxwait.
     
     
    ax25 mycall [<ax25call>]

     Display or set the default local AX.25 address.  The standard
     format is used, (e.g. WG7J or KA7EHK-5).  This command must be
     given before any attach commands using AX.25 mode are given.
          
          ax25 mycall wg7j-3
          
          
    ax25 paclen [<size>]

     This sets the default paclen used when attaching interfaces
     that will carry AX.25 connections. See also 'ifconfig <iface> ax25
     paclen'. Default is 256 bytes.
     This parameter limits the size of I-fields on new AX.25
     connections.  If IP datagrams or fragments of datagrams larger
     than paclen are transmitted, they will be transparently
     fragmented at the AX.25 level, sent as a series of I frames, and
     reassembled back into a complete IP datagram or fragment at the
     other end of the link.  IP datagrams will not be affected if this
     parameter is greater than or equal to the MTU of the associated
     interface.
     If NET/ROM communication is configured, the NetRom MTU value
     should be Paclen - 20. !!!  The Net/Rom header takes 20 bytes,
     and is part of the AX25 data.  Default netrom mtu is 236.
     
     Note1:  the AX.25 Level 2 Version 2 definition specifies a
     maximum paclen of 256 bytes. Some systems are not equipped to
     handle larger packets (e.g. G8BPQ based systems), so be careful
     when using this parameter.
     
     Note2:  see also the section 'Of PACLEN, MTU, MSS, and More'
     in the JNOS40 Configuration Manual.
     

    ax25 pthresh [<size>]

     Display or set the poll threshold to be used for new AX.25
     Version 2 connections.  The poll threshold controls
     retransmission behavior as follows. If the oldest unacknowledged
     I-frame size is less than the poll threshold, it will be sent
     with the poll (P) bit set if a time-out occurs. If the oldest
     unacked I-frame size is equal to or greater than the threshold,
     then a RR or RNR frame, as appropriate, with the poll bit set
     will be sent if a time-out occurs.
     The idea behind the poll threshold is that the extra time needed
     to send a "small" I-frame instead of a supervisory frame when
     polling after a time-out is small, and since there's a good
     chance the I-frame will have to be sent anyway (i.e., if it were
     lost previously) then you might as well send it as the poll.  But
     if the I-frame is large, send a supervisory (RR/RNR) poll instead
     to determine first if retransmitting the oldest unacknowledged I-
     frame is necessary; the time-out might have been caused by a lost
     acknowledgment.  This is obviously a tradeoff, so experiment with
     the poll threshold setting. The default is 128 bytes, one half
     the default value of <paclen>
     

    ax25 reset <axcb>

     Delete the AX.25 connection control block at the specified
     address. This deletes a connection and everything associated with
     it. The control block address can be found with the 'ax25 status'
     command.
     

    ax25 retries [<count>]

     Limit the number of successive unsuccessful retransmission
     attempts  on  new AX.25  connections.  If this  limit is
     exceeded, link re-establishment is attempted.  If the link can't
     be re-established in <count> times, then the connection  is
     abandoned and all queued data is deleted.  Default is 5.
          

    ax25 route [<subcommand>]

     Without optional subcommands, display the AX.25 routing table
     that specifies the digipeaters to be used  in reaching a given
     station.


       ax25 route add <target> <iface> [[via] digis ...]
       ax25 route perm <target> <iface> [[via] digis ...]

        Add an entry to the AX.25 routing table.  The route added may
        be replaced automatically by a new one, unless "perm" is used
        instead of "add".  Replacement may occur when digipeaters are
        specified in an AX25 link from the node or a connection is
        received from a remote station via digipeaters.  Such automatic
        replacement is usually desirable; use "route add perm ..." to
        prevent this where necessary.

        <target> is the destination call to reach via digipeaters.
        <iface> is the interface to use for this route, i.e. Jnos allows
           different digi routes for different interfaces.
        [digis...] is a list of one or more digipeaters, separated by
           spaces and/or commas.  The keyword "via" is optional.
          
          ax25 route add k7uyx-1 port1 wg7j wa7tas n7dva
          ax25 route perm k7uyx-1 port1 wg7j wa7tas n7dva
          
          
       ax25 route drop <target> <iface>

        Drop an entry for <target> from the AX.25 routing table.

                    ax25 route drop k7uyx-1 port1
          
          
       ax25 route mode <target> <iface> [vc|dg|interface]

        Sets the interface ip mode to one of  vc | datagram |
        interface for target.  This indicates how ip links to the
        destination call <target> should be established.  If nothing
        is given for a certain destination or target, the interface
        default mode is used, which defaults to datagram.  (See also
        the 'mode' command).
        vc        is a virtual circuit (ax25 connected mode, meaning
                  that ip frames are sent using ax.25 connections)
        datagram  is unconnected mode, (AX25 UI frames).
        interface is the default interface mode, as set with the
                  'mode' command.
        
          ax25 route mode k7uyx-1 port1 vc
          

    ax25 status [<axcb>]

     Without an argument, display a one-line summary of each AX.25
     control block.  If the address of a particular control block is
     specified, the contents of that control block is shown in more
     detail. Note that the send queue units are frames, while the
     receive queue units are bytes.
     

    ax25 t2 [<milliseconds>]                           Default: 1000

     Display or set the AX.25 "resptime" timer. Value is in milli-
     seconds. Default is 1000ms, and minimum is 0ms.  This timer lets
     Jnos eliminate some redundant transmissions and optimize what it
     sends by possibly adding ACKs to I-frames, by delaying the trans-
     mission of packets.  A value of zero disables the use of the t2
     timer, as does the use of datagram mode (UI) [see mode command].
     

    ax25 t3 [<milliseconds>]                           Default: 0

     Display or set the AX.25 idle "keep alive" timer. Value is
     in milliseconds. Default is 0, i.e. no 'keep-alive' polling.
     

    ax25 t4 [<seconds>]                                Default: 300

     Display or set the AX.25 Link "redundancy" timer. Value is
     in seconds. When no exchange has been had during this time the
     link is reset and closed. Default = 300 seconds (5 minutes).
     

    ax25 timertype [linear|EXPONENTIAL|original]

     Sets or displays the type of timer used for retransmission
     and recovery.  Linear means that each retry will use a time-out
     that is RTT greater then the previous time-out. I.e. 4 sec, 8
     sec, 12 sec, 16 seconds etc.  Exponential means that each retry
     will use a time-out that is twice as large as the previous time-
     out. I.e. 4 seconds, 8 seconds, 16 seconds, 32 seconds etc.
     Original means that each retry will use a time out that is twice
     the RTT, i.e. 4 seconds, 8 seconds, 8 seconds, 8 seconds, etc.
     Default is exponential.
     
          ax25 timertype linear


    ax25 ttycall [ttycall]

     Set or display the tty-link callsign for direct keyboard access.
     Remember to have both 'attended on' and 'mbox attend on' to be
     able to use this function.  The ttycall value is also used for the
     source address of ax.25 connections initiated from the console,
     provided that Jnos was compiled with TTYCALL_CONNECT #define'd.
     
     
    ax25 version [n]

     Display or set the version of the AX.25 protocol to attempt
     to  use  on  new connections.  Version 1 is the version that does
     not use the poll/final bits.  Default is version 2.
     

    ax25 window [<size>]                             Default: 512 bytes

     Set the number of bytes that can be pending on an AX.25
     receive queue  beyond which  I  frames will be answered with RNR
     (Receiver Not Ready) responses. This presently applies only to
     suspended interactive  AX.25  sessions,  since incoming  I-frames
     containing  network (IP, NET/ROM) packets are always processed
     immediately and are not placed on the receive queue.  However,
     when an AX.25 connection carries both interactive and network packet
     traffic, an RNR generated because of backlogged interactive traffic
     will also stop network packet traffic from being sent.

     

