
popmail <subcommands>

    The following popmail subcommands describe how to configure a POP2
    or POP3 client daemon.  A Jnos POP server host need only 'start' the
    pop2 or pop3 daemon, and configure the popusers file, to make his
    system accessible to pop clients.


    popmail addserver <host> [<seconds>] [hh:mm-hh:mm] <protocol>
    <mailbox> <username> [<password>]

     Add hostP as a pop server. When seconds is given, a timer is
     started to query the  host with that interval for mail. If not
     specified no querying to the pop host will be started. You have
     to do that manually with a kick.  When  hh:mm is given then only
     in that exact timeframe are queries to the host made (allowed).
     Protocol is either POP2 or POP3, depending on the  mail service
     the host is providing.

     Note: pop2 is superceded by pop3.

     Mailbox is the mailbox name on the local host where mail is to be
     stored.
     
     Username and password are this system's validation parameters for
     the host.  If password is omitted, it is obtained from the console.

     Note: On entering this command the host name is looked up. If
     nonexistent, an error message is displayed.


    popmail dropserver <host> [<username>]

     Drops host from the list of pop servers to be queried.  All
     references to the entry are deleted from the current system.  If
     <username> is supplied, only the server entry having the specified
     username is dropped.


    popmail kick <host> [<username>]

     Starts a pop session with host to retrieve mail.  This command is
     needed  when no interval is specified with the popmail addserver
     command.  If <username> is supplied, only the server entry having
     the specified username is kicked.


    popmail list

     Lists the current popmail server table.


    popmail quiet <yes|NO>

     Enables or disables the inclusion of a beep in the message that
     new mail arrived at this system via pop.  See 'popmail trace'
     below, to enable the printing of a new-mail-arrived message.


    popmail t4 [<#seconds>]                               Default: 0

     Displays or sets the pop client's idle timeout value (in seconds).
     Zero means no timeout, otherwise the value should be at least 300
     seconds.  This command is only available when POPT4 was #define'd
     when Jnos was compiled.


    popmail trace <level>                                 Default: 1

     Displays or sets the trace level of pop sessions. Current trace
     levels are:
     
      0 - No tracing
      1 - Serious errors reported
      2 - Transient errors reported
      3 - session progress reported
     
     Note that tracing only goes to the log file, and that a non-zero
     trace value also causes a "new mail arrived" message to be displayed
     on the console.


    popmail lzw [off | ON]

     Displays or sets the LZW compression enable flag.
