
axui  <iface>  [<unproto_call> [digipeater_string]]

     The axui command allows the console user to create a session that
sends UI (unproto) packets to <unproto_call> via interface <iface>, from
lines entered at the keyboard, and displays received UI packets.  A split-
screen session is created where possible.  The default <unproto_call> is
"ID".  At most one axui session is permitted.  AXUISESSION must be defined
in config.h to enable this command.

An axui session also allows a few commands, which begin with '/' in column 1:

/c newcall      change the unproto call to <newcall>
/i iface        change to interface <iface>
/? or /h        display a help message
/t              toggle display of the time a packet was received
/q, /b, /e      close the axui session

Example:
     To use digipeater n5knx-5 for a local ARES unproto net, type:
axui ax0 ares n5knx-5
