
smtp <subcommand>


     These commands are used for the Simple  Message  Transport
     Protocol  service (that is, mail).

    smtp batch [yes | no]

     If set smtp will batch the commands into one frame.  When off
     only one command is sent and a response is waited for.  Some old
     and flaky smtp servers cannot handle more than one command at a
     time.  NOS can handle multiple.  If you are not hindered by an
     old smtp server, setting batch reduces bandwidth.  However, if you
     obtain bounced email containing:
         >>> DATA
         <<< 503 Need MAIL before RCPT
     you should turn batching off!


    smtp dtimeout [<hours>]                             Default: 0
     Displays or sets the number of hours a message will remain in
     the smtp mqueue before being returned to sender.  Delivery attempts
     are made at "smtp timer" intervals.  If <hours> is zero, the message
     remains in mqueue indefinitely.


    smtp gateway [<hostid> | none]

     Displays or sets the host to be used as a "smart" mail relay.
     Any  mail sent to a host not in the domain.txt file or not found
     via a nameserver query, will instead be sent to the gateway for
     forwarding (if defined).  To undefine the gateway, specify "none".
     The gateway name is resolved into an IP address each time the outgoing
     mail queue is processed,  to allow for DNS changes.


    smtp hopper [ON | off]

     Displays or sets the flag used to enable the "hopper" feature.
     This feature, available when Jnos is compiled with HOPPER #define'd,
     will attempt to deliver mail to a router which serves the mail's
     destination address.  The router address is used even when an
     'smtp gateway' is defined.  This feature should not be enabled when
     X1J or other routers are used which don't accept SMTP connections.
     Note that if HOPPER is #defined, then Jnos starts with smtp hopper
     enabled by default.  This feature is inherently "dangerous"!


    smtp kick [<hostname>]

     Run through the outgoing mail queue and attempt to deliver any
     pending mail to all systems, or just to <hostname> if specified.
     This command allows the user to "kick" the mail system manually.
     Normally, this command is periodically invoked by a timer whenever
     NOS is running.


    smtp kill <jobid>

     Kill <jobid> and delete the message.  If the job is "locked" by
     the smtp client process, you will be asked to confirm the removal.


    smtp list

     List the current jobs in the mqueue. An "L" means locked and in progress.
     It is wise to add in autoexec.bat a "del /spool/mqueue/*.lck" command,
     since Jnos will not remove any pre-existing locks (it assumes other
     tasks share the mqueue, and dare not remove their locks).


    smtp maxclients [<count>]

     Displays or sets the maximum number of simultaneous outgoing
     SMTP sessions that will be allowed.  The default is 10.  Reduce
     <count> if network congestion is a problem.


    smtp maxservers [<count>]

     Displays or sets the maximum number of simultaneous incoming
     SMTP sessions that will be allowed.  The default is 0, ie, no
     limit other than the amount of memory available.


    smtp mode [queue | ROUTE]

     Sets the smtp delivery mode.  If 'queue', all messages are left
     in /spool/rqueue for external forwarding and handling.  If
     'route', messages are handled and, if for local, appended to a
     mailbox, or if remote they are forwarded.  Default = 'route'


    smtp quiet [YES | no]

     Enables or disables the inclusion of a beep in the message that
     new mail arrived at this system.  See 'smtp trace' below, to
     enable the printing of a new-mail-arrived message.


    smtp t4 [<seconds>]

     Displays or sets a t4 timer for smtp client (outgoing) sessions
     so that they will disconnect after a period of inactivity and
     prevent lockups.  Default = 0, i.e., no disconnect timeout.
     

    smtp tdisc [<seconds>]

     Displays or sets a disconect timer for smtp server (incoming) 
     sessions so that they will disconnect after a period of inactivity
     and prevent lockups. Default = 0, i.e., no disconnect timeout.


    smtp timer [<seconds>]

     Displays or sets the interval, between scans of the outbound mail
     queue.  For example, smtp timer 600 will cause the system to
     check for outgoing mail every 10 minutes and attempt to deliver
     anything it finds, subject of course to the smtp maxclients
     limit.  Setting a value of zero disables queue scanning
     altogether.  This value is  recommended  for standalone IP
     gateways that never handle mail, since it saves wear and tear on
     the (floppy) disk drive.  Default = 0
    

    smtp trace [<value>]

     Displays or sets the trace flag in the SMTP client, allowing you
     to  watch SMTP's conversations as it delivers mail.  Zero (the
     default) disables tracing.  A trace value of 1 just enables the
     "new mail for n5knx from <k5arh@w5ddl.ampr.org>".  Larger values
     produce more voluminous trace output.
     

    smtp usemx [yes | NO]

     Displays or sets a flag enabling or disabling MX record lookups.
     This can  be enabled if a domain server is available in the near
     distance (reachable).  It should be disabled (default) if no
     domain server is in reach to  satisfy  the MX  query.  Note that
     MX record handling is limited to the five highest-preference hosts.
     Also, the smtp t4 timer must be set, in order to timeout on non-
     responsive hosts.

     Note that the default behavior of Jnos MX lookup, is to give precedence
     to a wildcard-MX RR (resource-record) over an explicit A RR.  Wildcard-MX
     RRs are somewhat rare, and come into play when no explicit MX RR exists
     for a host.  If using a wildcard-MX is inappropriate, one could either
     add an explicit MX RR for the host in question, or recompile Jnos with
     SMTP_A_BEFORE_WILDMX #define'd so as to change the lookup precedence.
