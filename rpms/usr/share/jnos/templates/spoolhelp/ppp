
ppp


The 'ppp' commands are used for Point to Point Protocol
interfaces.

This implementation of PPP is designed to be as complete as
possible.  Because of this, the number of options can be rather
daunting.  However, a typical PPP configuration might include the
following commands:

        attach asy 0x3f8 4 ppp pp0 4096 1500 9600
        dial pp0 dialer.pp0 30 3 <remotehost>
        #
        ppp pp0 quick
        ppp pp0 lcp open active
        #
	#Note: if remote assigns you a dynamic IP addr, you may need this cmd:
	ppp pp0 ipcp local address none
        ppp pp0 ipcp open active
        #
        ppp pp0 pap user authname authpasswd
        # 
        route add default pp0
        #Note: if you are a Server, change 'open active' to 'open passive'
        #      in two places, above, and also issue this command to force
        #      clients to use pap:  ppp pp0 lcp local authenticate pap


ppp <interface>
  Display the status of the PPP interface.


ppp <interface> quick
  A quick way of issuing these commands:
  ppp <interface> lcp local accm 0
  ppp <interface> lcp local magic (misc_value)
  ppp <interface> lcp local acfc on
  ppp <interface> lcp local pfc on
  ppp <interface> ipcp local compress tcp 16


The following commands are used for the LCP (Link Control
Protocol) configuration.

ppp <interface> lcp close
  Shutdown the PPP interface.


ppp <interface> lcp local ...
  The 'lcp local' commands control the configuration of the local
  side of the link.  If an option is specified, the parameters will
  be used as the initial values in configuration requests.  If not
  specified, that option will not be requested.

  For each of these options, the 'allow' parameter will permit the
  remote host to include that option in its response, even when the
  option is not included in the request.  By default, all options
  are allowed.


ppp <interface> lcp local accm [ <bitmap> | allow [on | off] ]
  Display or set the ACCM (Async Control Character Map).  The
  default is 0xffffffff.


ppp <interface> lcp local authenticate [ pap | none | allow  [on | off] ]
  Display or set the authentication protocol.  The default is none.


ppp <interface> lcp local acfc [ on | off | allow [on | off] ]
  Display or set the option to compress the address and control
  fields of the PPP HLDC-like header.  This is generally desirable
  for slow asynchronous links, and undesirable for fast or
  synchronous links.  The default is off.


ppp <interface> lcp local pfc [on | off | allow [on|off]]
  Display or set the option to compress the protocol field of the
  PPP HLDC-like header.  This is generally desirable for slow
  asynchronous links, and undesirable for fast or synchronous
  links.  The default is off.


ppp <interface> lcp local magic [ on | off | <value> | allow [on|off] ]
  Display or set the initial Magic Number.  The default is off


ppp <interface> lcp local mru [ <size> | allow [on | off] ]
  Display or set the Maximum Receive Unit.  The default is 1500.


ppp <interface> lcp local default
  Reset the options to their default values.


ppp <interface> lcp open active | passive
  Wait for the physical layer to come up.  If 'active', initiate
  configuration negotiation.  If 'passive', wait for configuration
  negotiation from the remote.


ppp <interface> lcp remote ...
  The 'lcp remote' commands control the configuration of the remote
  side of the link.  The options are identical to those of the
  local side.

  If an option is specified, the parameters will be used in
  responses to the remote's configuration requests.

  If not specified, that option will be accepted if it is allowed.

  For each of these options, the 'allow' parameter will permit the
  remote to specify that option in its request.

  By default, all options are allowed.


ppp <interface> lcp timeout [<seconds>]                Default: 3
  Display or set the interval to wait between configuration or
  termination attempts.


ppp <interface> lcp try ...
  The 'lcp try' commands are used for the various counters.


ppp <interface> lcp try configure [<count>]           Default: 10
  Display or set the number of configuration requests sent.


ppp <interface> lcp try failure [<count>]              Default: 5
  Display or set the number of bad configuration requests allowed
  from the remote.


ppp <interface> lcp try terminate [<count>]            Default: 2
  Display or set the number of termination requests sent before
  shutdown.


ppp <interface> ipcp ...
  The 'ipcp' commands are used for the Internet Protocol Control
  Protocol configuration.

  The 'close', 'open', 'timeout' and 'try' sub-commands are
  identical to the LCP (described above).


ppp <interface> ipcp local ...
  The 'ipcp local' commands control the configuration of the local
  side of the link.  If an option is specified, the parameters will
  be used as the initial values in configuration requests.  If not
  specified, that option will not be requested.

  For each of these options, the 'allow' parameter will permit the
  remote to include that option in its response, even when the
  option is not included in the request.  By default, all options
  are allowed.


ppp <interface> ipcp local address [ <hostid> | none | allow [on | off] ]
  Display or set the local address for negotiation purposes.  If the
  address "none" (or 0) is specified, the other side of the link will supply
  the address.  By default, no addresses are negotiated.


ppp <interface> ipcp local compress [ tcp <slots>
             [<flag>] | none | allow [on | off] ]
  Display or set the compression protocol.  The default is none.

  The tcp <slots> specifies the number of "conversation" slots,
  which must be 1 to 255. (This may be limited at compilation time
  to a smaller number.)  A good choice is in the range 4 to 16.

  The tcp <flag> is 0 (don't compress the slot number) or 1 (OK to
  compress the slot number).  KA9Q NOS can handle compressed slot
  numbers, so the default is 1.


ppp <interface> ipcp pool [<address> [<count>]]
  The ipcp pool command is used to display or set a range (or pool)
  of consecutive IP addresses to be assigned to the PPP peer hosts.
  <count> defaults to 1.  Addresses are assigned from this pool
  unless the username provided by the peer host is found in FTPUSERS,
  is authorized, and contains an IP address (or equivalent hostname)
  in the fifth field, that is, following the permissions field.
  Note: this field format may vary for non-ppp-intended entries in
  FTPUSERS.


ppp <interface> ipcp remote ...
  The 'ipcp remote' commands control the configuration of the
  remote side of the link.  The options are identical to those of
  the local side.  If an option is specified, the parameters will
  be used in responses to the remote's configuration requests.  If
  not specified, that option will be accepted if it is allowed.
  For each of these options, the 'allow' parameter will permit the
  remote to specify that option in its request.  By default, all
  options are allowed.


ppp <iface> pap ...
  The 'pap' commands are used for the Password Authentication
  Protocol configuration.

  The 'timeout' and 'try' sub-commands are identical to the LCP
  (described above).  However, the terminate counter is unused.


ppp <interface> pap user [ <username> [<password>] ]
  Display or set the <username>.

  The <password> may also be set, but not displayed.

  When <username> is specified, but no password is supplied, the
  FTPUSERS file is searched for the password.  <username> must have
  the PPP_PWD_LOOKUP (=0x200) privilege bit set to allow the password to
  be taken from the FTPUSERS file.   When a username- password pair is
  unknown or rejected, a session will appear at the console to
  prompt for a new username-password.

  When the username-password combination is verified on the remote
  side of the link, the entry in the other system's FTPUSERS file
  (provided the remote system runs Jnos, of course) must have the
  PPP_ACCESS_PRIV (=0x100) privilege bit set, or the authorization
  attempt is refused.

ppp <interface> trace [<flags>]
  Display or set the flags that control the logging of information
  during PPP link configuration.

  The flag values are:

  0:  No trace
  1:  Basic trace
  2:  General trace

  Values greater than 2 are usually not compiled, and are described
  in the appropriate source files where they are defined.


Here are some selected PPP reference documents:
RFC:  TITLE:
1661  The Point-to-Point Protocol (PPP)
1663  PPP Reliable Transmission


Here are some usage tips:
1.  The dialer need not be used, as ' comm pp0 "ATDT5551212" ' could
    dial your ppp provider.  The dialer is of course much more flexible,
    and is necesary when a dialog is required with the provider.

2.  If your hostname from the ppp provider is dynamic (ie, it may
    change as your assigned IP address changes), you can assigned it
    dynamically in this fashion:   domain addserver <ip_addr>
                                   hostname pp0
    where you must replace <ip_addr> with the provider's DNS address.

3.  When DCD drops, Jnos will drop DTR and RTS to the modem.  This may
    prevent the modem from answering a callback by a remote ppp client,
    unless RingIndicator (RI, pin 22) is wired between the modem and the
    serial port.  A RI status will cause Jnos to do an automatic 'param UP'
    so as to reassert DTR and RTS, and thereby allow the modem to answer.

4. Here is a sample script to shutdown ppp, from W0PLW:
   ppp pp0 lcp close
   pause 2
   domain drop <your-dns-server-ip-addr-here>
   route drop default pp0
   popmail dropserver <your-pop-server-name>
   smtp gateway none
   detach pp0

