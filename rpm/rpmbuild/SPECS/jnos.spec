Summary:        Packet BBS
Name:           jnos
Version:        2.0j.3
Release:        1%{?dist}
License:        GPLv2
Group:          System Environment/Base
URL:            http://www.langelaar.net/projects/jnos2/
Source0:        %{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root
BuildArch:      armv6hl

BuildRequires:  ncurses-devel

%description
Raspberry Pi specific install of Maiko Langelaar's jnos 2.0.
Specific to Michigan, presumes TNC-Pi

%prep
%setup -q

%build
make

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_bindir}
install -m 755 jnos $RPM_BUILD_ROOT%{_bindir}
mkdir -p $RPM_BUILD_ROOT%{_datarootdir}/jnos
mkdir -p $RPM_BUILD_ROOT%{_datarootdir}/jnos/help
mkdir -p $RPM_BUILD_ROOT%{_datarootdir}/jnos/spoolhelp
install -m 644 usr/help/* $RPM_BUILD_ROOT%{_datarootdir}/jnos/help/
install -m 644 usr/spoolhelp/* \
 	$RPM_BUILD_ROOT%{_datarootdir}/jnos/spoolhelp/
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/jnos
install -m 644 etc/access.www $RPM_BUILD_ROOT%{_sysconfdir}/jnos/
install -m 644 etc/areas $RPM_BUILD_ROOT%{_sysconfdir}/jnos/
install -m 644 etc/forward.bbs $RPM_BUILD_ROOT%{_sysconfdir}/jnos/
install -m 644 etc/mail $RPM_BUILD_ROOT%{_sysconfdir}/jnos/
install -m 644 etc/names.dat $RPM_BUILD_ROOT%{_sysconfdir}/jnos/
install -m 644 etc/nos.cfg $RPM_BUILD_ROOT%{_sysconfdir}/jnos/
install -m 644 etc/rewrite $RPM_BUILD_ROOT%{_sysconfdir}/jnos/
install -m 644 etc/users.dat $RPM_BUILD_ROOT%{_sysconfdir}/jnos/

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_usr}/bin
%{_datarootdir}/jnos
%{_sysconfdir}/jnos

%changelog
* Tue Jun 11 2013 John J. McDonough <jjmcd@fedoraproject.org> - 2.0j.3-1
- Reorganize tar
* Tue Jun 11 2013 John J. McDonough <jjmcd@fedoraproject.org> - 2.0j.2-1
- Include templates in tar
* Tue Jun 11 2013 John J. McDonough <jjmcd@fedoraproject.org> - 2.0j.1-3
- Better use of macros
* Mon Jun 10 2013 John J. McDonough <jjmcd@fedoraproject.org> - 2.0j.1-2
- Correct protections on executable
* Mon Jun 10 2013 John J. McDonough <jjmcd@fedoraproject.org> - 2.0j.1-1
- yum localinstall --nogpgcheck 
-   http://ares-mi.org/repo/jnos-2.0j.1-2.rpfr.armv6hl.rpm
- Package Maikos work
