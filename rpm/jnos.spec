Summary:        Packet BBS
Name:           jnos
Version:        2.0j.1
Release:        3%{?dist}
License:        GPLv2
Group:          System Environment/Base
URL:            http://www.langelaar.net/projects/jnos2/
Source0:        %{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root
BuildArch:      armv6hl

BuildRequires:  ncurses-devel

%description
Raspberry Pi specific install of Maiko Langelaar's jnos 2.0

%prep
%setup -q

%build
make

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_bindir}
install -m 755 jnos $RPM_BUILD_ROOT%{_bindir}
mkdir -p $RPM_BUILD_ROOT%{_datarootdir}/jnos
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/jnos


%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_usr}/bin
%{_datarootdir}/jnos
%{_sysconfdir}/jnos

%changelog
* Tue Jun 11 2013 John J. McDonough <jjmcd@fedoraproject.org> - 2.0j.1-3
- Better use of macros
* Mon Jun 10 2013 John J. McDonough <jjmcd@fedoraproject.org> - 2.0j.1-2
- Correct protections on executable
* Mon Jun 10 2013 John J. McDonough <jjmcd@fedoraproject.org> - 2.0j.1-1
- yum localinstall --nogpgcheck 
-   http://ares-mi.org/repo/jnos-2.0j.1-2.rpfr.armv6hl.rpm
-Package Maikos work
