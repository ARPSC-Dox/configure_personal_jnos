[root@test1-l ~]# ./pitnc_getparams 0 0

01 TXDelay - Zero means use ADC 255
02 Persistance                   20
03 Slottime (in 10 mS)            4
04 TXTail                       100
05 Full Duplex - Not used         1
06 Our Channel (Hex)             00
07 I2C Address (0 = async) Hex   04
   ADC Value                     27
8 0 ff 14 4 64 1 0 4 1b 9d c0 sum 0 
[root@test1-l ~]# ./pitnc_setparams 0 0 1 250
8 0 fa 14 4 64 1 0 4 1b 98 c0 sum 0 

01 TXDelay - Zero means use ADC 250
02 Persistance                   20
03 Slottime (in 10 mS)            4
04 TXTail                       100
05 Full Duplex - Not used         1
06 Our Channel (Hex)             00
07 I2C Address (0 = async) Hex   04
   ADC Value                     27



[root@test1-l ~]# ./pitnc_setparams 0 0 1 100
8 0 64 14 4 64 1 0 0 3c 25 c0 sum 0 

01 TXDelay - Zero means use ADC 100
02 Persistance                   20
03 Slottime (in 10 mS)            4
04 TXTail                       100
05 Full Duplex - Not used         1
06 Our Channel (Hex)             00
07 I2C Address (0 = async) Hex   00
   ADC Value                     60
[

[root@test1-l ~]# ./pitnc_getparams 0 0

01 TXDelay - Zero means use ADC 100
02 Persistance                   20
03 Slottime (in 10 mS)            4
04 TXTail                       100
05 Full Duplex - Not used         1
06 Our Channel (Hex)             00
07 I2C Address (0 = async) Hex   00
   ADC Value                     60
8 0 64 14 4 64 1 0 0 3c 25 c0 sum 0 
[root@test1-l ~]# 


