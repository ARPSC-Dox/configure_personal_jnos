INSERT INTO `bugs` VALUES(
192,1,'','normal','ASSIGNED',now(),now(),'Image on website','None','P3',13,'Other',1, '1.0',145,'','default',NULL,'',0,now(),1,1,1,1.00,1.00,'2013-04-26 12:00:00',NULL);
INSERT INTO `bugs_fulltext` VALUES(192,'Image on website','Place completed Pidora plus JNOS SD image on ares-mi.org','Place completed Pidora plus JNOS SD image on ares-mi.org');
INSERT INTO `longdescs` VALUES(400,192,1,now(),'0.00','Place completed Pidora plus JNOS SD image on ares-mi.org',0,0,0,NULL);

INSERT INTO `bugs` VALUES(
193,1,'','normal','ASSIGNED',now(),now(),'Prepare image','None','P3',13,'Other',1, '1.0',145,'','default',NULL,'',0,now(),1,1,1,1.00,1.00,'2013-04-26 12:00:00',NULL);
INSERT INTO `bugs_fulltext` VALUES(193,'Prepare image','Make Pidora SD image including JNOS, Configure_Personal_JNOS and systemctl files','Make Pidora SD image including JNOS, Configure_Personal_JNOS and systemctl files');
INSERT INTO `longdescs` VALUES(401,193,1,now(),'0.00','Make Pidora SD image including JNOS, Configure_Personal_JNOS and systemctl files',0,0,0,NULL);

INSERT INTO `bugs` VALUES(
194,1,'','normal','ASSIGNED',now(),now(),'Test Web','None','P3',13,'Other',1, '1.0',145,'','default',NULL,'',0,now(),1,1,1,1.00,1.00,'2013-04-26 12:00:00',NULL);
INSERT INTO `bugs_fulltext` VALUES(194,'Test Web','Test JNOS web server configuration over the air as well as on the LAN','Test JNOS web server configuration over the air as well as on the LAN');
INSERT INTO `longdescs` VALUES(402,194,1,now(),'0.00','Test JNOS web server configuration over the air as well as on the LAN',0,0,0,NULL);

INSERT INTO `bugs` VALUES(
195,1,'','normal','ASSIGNED',now(),now(),'Configure web','None','P3',13,'Other',1, '1.0',145,'','default',NULL,'',0,now(),1,1,1,1.00,1.00,'2013-04-26 12:00:00',NULL);
INSERT INTO `bugs_fulltext` VALUES(195,'Configure web','Add JNOS web server to configuration and engine start','Add JNOS web server to configuration and engine start');
INSERT INTO `longdescs` VALUES(403,195,1,now(),'0.00','Add JNOS web server to configuration and engine start',0,0,0,NULL);

INSERT INTO `bugs` VALUES(
196,1,'','normal','ASSIGNED',now(),now(),'Configure CONVerse','None','P3',13,'Other',1, '1.0',145,'','default',NULL,'',0,now(),1,1,1,1.00,1.00,'2013-04-26 12:00:00',NULL);
INSERT INTO `bugs_fulltext` VALUES(196,'Configure CONVerse','Configure JNOS CONVerse bridge including proper county channel default','Configure JNOS CONVerse bridge including proper county channel default');
INSERT INTO `longdescs` VALUES(404,196,1,now(),'0.00','Configure JNOS CONVerse bridge including proper county channel default',0,0,0,NULL);

INSERT INTO `bugs` VALUES(
197,1,'','normal','ASSIGNED',now(),now(),'Test CONVerse','None','P3',13,'Other',1, '1.0',145,'','default',NULL,'',0,now(),1,1,1,1.00,1.00,'2013-04-26 12:00:00',NULL);
INSERT INTO `bugs_fulltext` VALUES(197,'Test CONVerse','Test JNOS CONVerse bridge over the air from another JNOS as well as from xconvers','Test JNOS CONVerse bridge over the air from another JNOS as well as from xconvers');
INSERT INTO `longdescs` VALUES(405,197,1,now(),'0.00','Test JNOS CONVerse bridge over the air from another JNOS as well as from xconvers',0,0,0,NULL);

INSERT INTO `dependencies` VALUES(197,196);
INSERT INTO `dependencies` VALUES(194,195);
INSERT INTO `dependencies` VALUES(192,193);
INSERT INTO `dependencies` VALUES(191,192);
INSERT INTO `dependencies` VALUES(191,197);
INSERT INTO `dependencies` VALUES(191,194);



INSERT INTO `bugs` VALUES(198,1,'','normal','ASSIGNED',now(),now(),'Convert for Maiko','None','P3',13,'Other',1, '1.0',145,'','default',NULL,'',0,now(),1,1,1,1.00,4.00,'',NULL);
INSERT INTO `bugs_fulltext` VALUES(198,'Convert for Maiko','Convert editscript and others to rely on VE4KLM style autoexec.nos','Convert editscript and others to rely on VE4KLM style autoexec.nos');
INSERT INTO `longdescs` VALUES(406,198,1,now(),'0.00','Convert editscript and others to rely on VE4KLM style autoexec.nos',0,0,0,NULL);

INSERT INTO `bugs` VALUES(199,1,'','normal','ASSIGNED',now(),now(),'Review WB8TKL config','None','P3',13,'Other',1, '1.0',145,'','default',NULL,'',0,now(),1,1,1,1.00,2.00,'',NULL);
INSERT INTO `bugs_fulltext` VALUES(199,'Review WB8TKL config','Review WB8TKL autoexec.nos for any issues that may have been overlooked','Review WB8TKL autoexec.nos for any issues that may have been overlooked');
INSERT INTO `longdescs` VALUES(407,199,1,now(),'0.00','Review WB8TKL autoexec.nos for any issues that may have been overlooked',0,0,0,NULL);

INSERT INTO `bugs` VALUES(200,1,'','normal','ASSIGNED',now(),now(),'Add user','None','P3',13,'Other',1, '1.0',145,'','default',NULL,'',0,now(),1,1,1,1.00,1.00,'',NULL);
INSERT INTO `bugs_fulltext` VALUES(200,'Add user','Include adding the jnos user to the installation script','Include adding the jnos user to the installation script');
INSERT INTO `longdescs` VALUES(408,200,1,now(),'0.00','Include adding the jnos user to the installation script',0,0,0,NULL);

INSERT INTO `bugs` VALUES(201,1,'','normal','ASSIGNED',now(),now(),'User logon files','None','P3',13,'Other',1, '1.0',145,'','default',NULL,'',0,now(),1,1,1,1.00,1.00,'',NULL);
INSERT INTO `bugs_fulltext` VALUES(201,'User logon files','Provide .bashrc and possibly others in a skel directory for useradd','Provide .bashrc and possibly others in a skel directory for useradd');
INSERT INTO `longdescs` VALUES(409,201,1,now(),'0.00','Provide .bashrc and possibly others in a skel directory for useradd',0,0,0,NULL);

INSERT INTO `bugs` VALUES(202,1,'','normal','ASSIGNED',now(),now(),'Add DNS','None','P3',13,'Other',1, '1.0',145,'','default',NULL,'',0,now(),1,1,1,8.00,1.00,'',NULL);
INSERT INTO `bugs_fulltext` VALUES(202,'Add DNS','Correct glade to request the default gateway and DNS, probably remove devices eth0 and ttyAMA0 as well as possibly MAC address','Correct glade to request the default gateway and DNS, probably remove devices eth0 and ttyAMA0 as well as possibly MAC address');
INSERT INTO `longdescs` VALUES(410,202,1,now(),'0.00','Correct glade to request the default gateway and DNS, probably remove devices eth0 and ttyAMA0 as well as possibly MAC address',0,0,0,NULL);

INSERT INTO `bugs` VALUES(
203,1,'','normal','ASSIGNED',now(),now(),'Enable systemctl','None','P3',13,'Other',1, '1.0',145,'','default',NULL,'',0,now(),1,1,1,1.00,1.00,'',NULL);
INSERT INTO `bugs_fulltext` VALUES(203,'Enable systemctl','Provide and set getty@tty6.service as well as execute systemctl enable command','Provide and set getty@tty6.service as well as execute systemctl enable command');
INSERT INTO `longdescs` VALUES(411,203,1,now(),'0.00','Provide and set getty@tty6.service as well as execute systemctl enable command',0,0,0,NULL);

INSERT INTO `dependencies` VALUES(199,198);
INSERT INTO `dependencies` VALUES(201,200);
INSERT INTO `dependencies` VALUES(195,198);
INSERT INTO `dependencies` VALUES(196,198);
INSERT INTO `dependencies` VALUES(193,201);
INSERT INTO `dependencies` VALUES(193,203);
INSERT INTO `dependencies` VALUES(193,202);
INSERT INTO `dependencies` VALUES();
INSERT INTO `dependencies` VALUES();
